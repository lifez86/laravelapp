## setup gitlab to run CI/CD 
https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/
https://docs.gitlab.com/ee/ssh/README.html#add-an-ssh-key-to-your-gitlab-account
https://docs.gitlab.com/ee/ci/ssh_keys/

## AWS EC2 Server setup

### php7.4-fpm

1. add php7.4-fpm configuration for laravelapp
```
sudo vi /etc/php/7.4/fpm/pool.d/laravelapp.conf
```
2. restart service
```
sudo systemctl restart php7.4-fpm
```
3. update php7.4-fpm config in nginx
```
# edit the sites-available file
sudo vi /etc/nginx/sites-available/laravelapp
# replace directory for fastcgi_pass
unix:/var/run/php/php7.4-fpm-laravelapp.sock;
```

### NGINX

```
webroot: /var/www/html/laravelapp
```

1. Create a script to disable / enable sites in nginx
https://serverfault.com/questions/424452/nginx-enable-site-command
https://www.javatpoint.com/steps-to-write-and-execute-a-shell-script

list all sites
```
sudo nginx_modsite.sh -l
```
enable website
```
sudo nginx_modsite.sh -e laravelapp
```
disable website
```
sudo nginx_modsite.sh -d laravelapp
```
reload nginx
```
sudo systemctl reload nginx
```
see nginx error logs
```
sudo tail -f /var/log/nginx/error.log
```

## Deployment setup with laravel envoy
https://medium.com/@lsfiege/how-to-deploy-a-laravel-app-with-envoy-inside-from-a-gitlab-ci-pipeline-d5b13906f35e

## Quick links

### Read error logs

1. laravel
```
vi /var/www/html/laravelapp/current/storage/logs/laravel.log
```

2. nginx error logs
```
vi /var/www/html/laravelapp/logs/error.log
```

3. nginx access logs
```
vi /var/www/html/laravelapp/logs/access.log
```