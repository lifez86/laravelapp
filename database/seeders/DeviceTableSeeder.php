<?php

namespace Database\Seeders;

use App\Device;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeviceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        //Device::truncate();
        DB::table('devices')->delete();
        DB::statement('ALTER TABLE `devices` AUTO_INCREMENT = 1');

        $faker = \Faker\Factory::create();

        //create a few devices with key and json value
        for ($i = 0; $i < 2; $i++) {
            Device::create([
                'device_key' => $faker->uuid,
                'device_value' => ['phone' => $faker->phoneNumber],
            ]);
        }

        //create a few devices with key and string value
        for ($i = 0; $i < 3; $i++) {
            Device::create([
                'device_key' => $faker->uuid,
                'device_value' => $faker->phoneNumber,
            ]);
        }
    }
}
