<?php

namespace Database\Seeders;

use App\Truck;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TruckTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        //Commands::truncate();
        DB::table('trucks')->delete();
        DB::statement('ALTER TABLE `trucks` AUTO_INCREMENT = 0');

        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Faker\Provider\Fakecar($faker));
        $entries = 10;

        for ($i = 0; $i < $entries; $i++) {
            Truck::create([
                'model' => $faker->vehicle,
            ]);
        }
    }
}
