<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $this->call(DeviceTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CommandTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(SGDistrictTableSeeder::class);
        $this->call(TruckTableSeeder::class);
        $this->call(CarStateTableSeeder::class); //requires TruckSeeder
        $this->call(TripTableSeeder::class); //requires CarStateTableSeeder
        $this->call(LatestLocationSeeder::class); //requires TruckSeeder
    }
}
