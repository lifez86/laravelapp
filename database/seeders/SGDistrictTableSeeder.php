<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Managers\DistrictManager;
use Illuminate\Support\Facades\DB;

class SGDistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('districts')->delete();
        DB::statement('ALTER TABLE `districts` AUTO_INCREMENT = 0');

        // save singapore districts into database, they are stored in /database/seeders/kml
        $dm = new DistrictManager();
        $dm->parseDataFromFile('district.kml');
        $dm->saveKMLData();
    }
}
