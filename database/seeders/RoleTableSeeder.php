<?php

namespace Database\Seeders;

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::statement('ALTER TABLE `roles` AUTO_INCREMENT = 1');

        //create a few roles
        Role::create([
            'name' => Role::ROLE_ADMIN,
        ]);

        Role::create([
            'name' => Role::ROLE_USER,
        ]);

        Role::create([
            'name' => Role::ROLE_DEV,
        ]);

        foreach (Role::all() as $role) {
            $users = User::inRandomOrder()->take(rand(1, 5))->pluck('id');
            $role->users()->attach($users);
        }
    }
}
