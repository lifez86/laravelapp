<?php

namespace Database\Seeders;

use App\Role;
use App\User;
use App\RoleUser;
use Illuminate\Database\Seeder;

class UserWithSetRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        //create admin
        $admin = User::firstOrCreate([
            'name' => 'Admin User',
            'email' => 'admin@laravelapp.com',
            'password' => '12345',
        ]);

        $admin_role = Role::firstOrCreate([
            'name' => Role::ROLE_ADMIN,
        ]);

        RoleUser::firstOrCreate([
            'user_id' => $admin->id,
            'role_id' => $admin_role->id,
        ]);

        //create user
        $user = User::firstOrCreate([
            'name' => 'User',
            'email' => 'user@laravelapp.com',
            'password' => '67890',
        ]);

        $user_role = Role::firstOrCreate([
            'name' => Role::ROLE_USER,
        ]);

        RoleUser::firstOrCreate([
            'user_id' => $user->id,
            'role_id' => $user_role->id,
        ]);
    }
}
