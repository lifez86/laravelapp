<?php

namespace Database\Seeders;

use App\Trip;
use App\Truck;
use App\Managers\TripManager;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TripTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        DB::table('trips')->delete();
        DB::statement('ALTER TABLE `trips` AUTO_INCREMENT = 0');

        $mgr = new TripManager();
        $truck = Truck::findOrFail(1);

        $timezone = new \DateTimeZone('Asia/Singapore');
        $startDate = Carbon::createFromFormat('Y-m-d H:i:s', '2021-04-26 00:00:00', $timezone);
        $endDate = Carbon::createFromFormat('Y-m-d H:i:s', '2021-04-26 23:59:59', $timezone);

        $lineString = $mgr->generateLineString($truck, $startDate, $endDate);
        if ($lineString) {
            Trip::create([
                'truck_id' => $truck->id,
                'start_date' => $startDate,
                'end_date' => $endDate,
                'route' => $lineString,
            ]);
        }
    }
}
