<?php

namespace Database\Seeders;

use App\Truck;
use Illuminate\Database\Seeder;
use App\Managers\CarStateManager;
use Illuminate\Support\Facades\DB;

class CarStateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        //Commands::truncate();
        DB::table('car_states')->delete();
        DB::statement('ALTER TABLE `car_states` AUTO_INCREMENT = 0');

        $truck = Truck::findOrFail(1);

        //seed list of GPS coords
        $csm = new CarStateManager();
        $csm->saveDataFromFile('GPS_FBG7949G_data_26042021.csv', $truck);
    }
}
