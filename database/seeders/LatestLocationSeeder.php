<?php

namespace Database\Seeders;

use App\Truck;
use App\LatestLocation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class LatestLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        DB::table('latest_locations')->delete();
        DB::statement('ALTER TABLE `latest_locations` AUTO_INCREMENT = 0');

        //get all trucks - should have 10 trucks from trucker seeder
        $trucks = Truck::all();

        $locations = [
            [1.3804959472613578, 103.76318749440958], //hillon mall
            [1.3341147614055053, 103.67910395102058], //sg discovery ctr
            [1.2876464091870374, 103.78245814193944], //hwa par villa
            [1.2926973584348604, 103.84683798954755], //clarke quay
            [1.2849472079826185, 103.86102770453792], //mbs
            [1.3072988200673088, 103.90627779996667], //communal place
            [1.3671098920767837, 103.99188739533093], //changi airport
            [1.4076593174847816, 103.90460314377545], //waterway pt
            [1.4362860324313103, 103.8217768408187], //hot springs
            [1.4059113146317699, 103.79302356057643], //sg zoo
        ];

        $loc_counter = 0;
        foreach ($trucks as $truck) {
            $point = new Point($locations[$loc_counter][0], $locations[$loc_counter][1]);
            LatestLocation::create([
                'truck_id' => $truck->id,
                'latlon' => $point,
            ]);
            $loc_counter++;
        }
    }
}
