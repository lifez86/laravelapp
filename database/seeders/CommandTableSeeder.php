<?php

namespace Database\Seeders;

use App\Device;
use App\Command;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        DB::table('commands')->delete();
        DB::statement('ALTER TABLE `commands` AUTO_INCREMENT = 0');

        $device = Device::firstOrCreate(
            ['device_key' => 'deviceWithCmds'],
            ['device_value' => 'deviceWithCmdsValue',
                'created_at' => now()->getTimestamp(),
                'updated_at' => now()->getTimestamp(),
            ]
        );

        $device2 = Device::firstOrCreate(
            ['device_key' => 'deviceWithCmds2'],
            ['device_value' => 'deviceWithCmdsValue2',
                'created_at' => now()->getTimestamp(),
                'updated_at' => now()->getTimestamp(),
            ]
        );

        $device3 = Device::firstOrCreate(
            ['device_key' => 'deviceWithCmds3'],
            ['device_value' => 'deviceWithCmdsValue3',
                'created_at' => now()->getTimestamp(),
                'updated_at' => now()->getTimestamp(),
            ]
        );

        // Factory seeding is way slower than DB::insert
        // Command::factory()->count(5000)->for($device)->create();
        // Command::factory()->count(5000)->for($device2)->create();
        // Command::factory()->count(5000)->for($device3)->create();

        $cmdName = [Command::DESC_LOCK, Command::DESC_UNLOCK];
        $resultName = [Command::RESULT_OK, Command::RESULT_NOK];
        $faker = \Faker\Factory::create();
        $data = [];

        for ($i = 0; $i < 5000; $i++) {
            $data[] = [
                'desc' => $cmdName[rand(0, count($cmdName) - 1)],
                'result' => $resultName[rand(0, count($resultName) - 1)],
                'txnref' => $faker->uuid,
                'device_id' => $device->id,
                'device_updated_at' => now()->getTimestamp(),
                'created_at' => now()->getTimestamp(),
                'updated_at' => now()->getTimestamp(),
            ];

            $data2[] = [
                'desc' => $cmdName[rand(0, count($cmdName) - 1)],
                'result' => $resultName[rand(0, count($resultName) - 1)],
                'txnref' => $faker->uuid,
                'device_id' => $device2->id,
                'device_updated_at' => now()->getTimestamp(),
                'created_at' => now()->getTimestamp(),
                'updated_at' => now()->getTimestamp(),
            ];

            $data3[] = [
                'desc' => $cmdName[rand(0, count($cmdName) - 1)],
                'result' => $resultName[rand(0, count($resultName) - 1)],
                'txnref' => $faker->uuid,
                'device_id' => $device3->id,
                'device_updated_at' => now()->getTimestamp(),
                'created_at' => now()->getTimestamp(),
                'updated_at' => now()->getTimestamp(),
            ];
        }

        $chunks = array_chunk($data, 1000);
        $chunks2 = array_chunk($data2, 1000);
        $chunks3 = array_chunk($data3, 1000);

        foreach ($chunks as $chunk) {
            Command::insert($chunk);
        }

        foreach ($chunks2 as $chunk2) {
            Command::insert($chunk2);
        }

        foreach ($chunks3 as $chunk3) {
            Command::insert($chunk3);
        }
    }
}
