<?php

namespace Database\Factories;

use App\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $roles = [
            Role::ROLE_ADMIN, Role::ROLE_DEV, Role::ROLE_USER,
        ];

        $randomRole = rand(0, count($roles) - 1);

        return [
            'name' => $roles[$randomRole],
        ];
    }
}
