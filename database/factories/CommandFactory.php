<?php

namespace Database\Factories;

use App\Command;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommandFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Command::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $cmdName = [Command::DESC_LOCK, Command::DESC_UNLOCK];
        $resultName = [Command::RESULT_OK, Command::RESULT_NOK];

        return [
            'desc' => $cmdName[rand(0, count($cmdName) - 1)],
            'result' => $resultName[rand(0, count($resultName) - 1)],
            'txnref' => $this->faker->uuid,
            'device_updated_at' => now()->getTimestamp(),
        ];
    }
}
