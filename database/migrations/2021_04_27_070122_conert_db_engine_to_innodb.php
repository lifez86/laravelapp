<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class ConertDbEngineToInnodb extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $tables = [
            'device',
            'users',
            'failed_jobs',
            'migrations',
            'password_resets',
        ];

        foreach ($tables as $table) {
            DB::statement('ALTER TABLE '.$table.' ENGINE = InnoDB');
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        $tables = [
            'device',
            'users',
            'failed_jobs',
            'migrations',
            'password_resets',
        ];

        foreach ($tables as $table) {
            DB::statement('ALTER TABLE '.$table.' ENGINE = MyISAM');
        }
        Schema::enableForeignKeyConstraints();
    }
}
