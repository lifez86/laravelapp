<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('districts', function (Blueprint $table) {
            $table->index('name');
            $table->index('country');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('districts', function (Blueprint $table) {
            $table->dropIndex('name');
            $table->dropIndex('country');
        });
    }
}
