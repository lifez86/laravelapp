<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLatestLocationsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('latest_locations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('truck_id')
                ->unique()
                ->constrained()
                ->onDelete('cascade');
            $table->point('latlon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('latest_locations');
        Schema::enableForeignKeyConstraints();
    }
}
