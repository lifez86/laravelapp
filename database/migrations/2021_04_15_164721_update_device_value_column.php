<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDeviceValueColumn extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('device', function (Blueprint $table) {
            $table->json('device_value')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('device', function (Blueprint $table) {
            $table->string('device_value')->nullable()->change();
        });
    }
}
