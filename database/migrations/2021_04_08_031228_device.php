<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Device extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('device', function (Blueprint $table) {
            $table->id();
            $table->string('device_key');
            $table->string('device_value')->nullable();
            //$table->timestamps();
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('updated_at');
            $table->index(['device_key', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('device');
    }
}
