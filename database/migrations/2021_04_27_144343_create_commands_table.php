<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::rename('device', 'devices');

        Schema::create('commands', function (Blueprint $table) {
            $table->id();
            $table->string('desc');
            $table->string('result');
            $table->string('txnref');
            $table->foreignId('device_id')
                ->constrained()
                ->onDelete('cascade');
            $table->unsignedInteger('device_updated_at');
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('updated_at');
            $table->index(['device_id', 'updated_at']);
            $table->index(['device_id', 'device_updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::rename('devices', 'device');
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('commands');
        Schema::enableForeignKeyConstraints();
    }
}
