<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarStatesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('car_states', function (Blueprint $table) {
            $table->id();
            $table->foreignId('truck_id')
                ->constrained()
                ->onDelete('cascade');
            $table->point('latlon');
            $table->timestamps();
            $table->index(['truck_id', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('car_states');
        Schema::enableForeignKeyConstraints();
    }
}
