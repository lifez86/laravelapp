<?php

namespace Tests;

use App\Role;
use App\User;
use App\Device;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUsersWithRoles(): void
    {
        $faker = \Faker\Factory::create();

        // create users
        User::factory()->count(5)->create(['password' => $faker->password(20)]);

        // create a few roles
        Role::factory()->create(['name' => Role::ROLE_ADMIN]);
        Role::factory()->create(['name' => Role::ROLE_USER]);
        Role::factory()->create(['name' => Role::ROLE_DEV]);

        foreach (Role::all() as $role) {
            $users = User::inRandomOrder()->take(rand(1, 5))->pluck('id');
            $role->users()->attach($users);
        }
    }

    public function createAdmin(): User
    {
        $user = User::factory()->create(['email' => 'admin@laravelapp.com']);
        $role = Role::factory()->create(['name' => Role::ROLE_ADMIN]);

        $role->users()->attach([$user->id]);

        return $user;
    }

    public function createUser(): User
    {
        $user = User::factory()->create(['email' => 'user@laravelapp.com']);
        $role = Role::factory()->create(['name' => Role::ROLE_USER]);

        $role->users()->attach([$user->id]);

        return $user;
    }

    public function createDevices(): void
    {
        $faker = \Faker\Factory::create();

        //create a few devices with key and json value
        Device::factory()->count(2)->create([
            'device_key' => $faker->uuid,
            'device_value' => ['phone' => $faker->phoneNumber],
        ]);

        //create a few devices with key and string value
        Device::factory()->count(3)->create([
            'device_key' => $faker->uuid,
            'device_value' => $faker->phoneNumber,
        ]);
    }
}
