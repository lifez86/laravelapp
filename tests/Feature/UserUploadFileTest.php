<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Tests\CreatesApplication;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class UserUploadFileTest extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;
    use WithoutMiddleware;

    public User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create(['email' => 'charlie@drive.sg']);
    }

    public function testUploadJpegFile()
    {
        $file = UploadedFile::fake()->image('avatar.jpg');

        $this->testUploadImageFile($file);
    }

    public function testUploadPngFile()
    {
        $file = UploadedFile::fake()->image('avatar.png');

        $this->testUploadImageFile($file);
    }

    public function testUploadJpegFileExceedFilesize()
    {
        //file size more than 10mb
        $file = UploadedFile::fake()->image('avatar.jpg')->size(50000);

        Storage::fake('s3');

        $response = $this->postJson("/api/users/{$this->user->id}/upload", [
            'uploadedFile' => $file,
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['uploadedFile']);
    }

    public function testUploadJpegFileIncorrectFormat()
    {
        //file size more than 10mb
        $file = UploadedFile::fake()->image('avatar.pdf');

        Storage::fake('s3');

        $response = $this->postJson("/api/users/{$this->user->id}/upload", [
            'uploadedFile' => $file,
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['uploadedFile']);
    }

    public function testUploadJpegEmptyField()
    {
        $response = $this->postJson("/api/users/{$this->user->id}/upload", []);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['uploadedFile']);
    }

    public function testUploadedFileReplaceExistingFile()
    {
        //fake existing file for user
        Storage::fake('s3');
        $file = UploadedFile::fake()->image('avatar.jpg');
        $fakedir = "uploadedFileDir/{$this->user->id}/";
        $fakeSavedFilename = 'test-file.png';
        $fakepath = $fakedir.'/'.$fakeSavedFilename;
        Storage::disk('s3')->putFileAs($fakedir, $file, $fakeSavedFilename);
        $this->user->profilePic = $fakepath;
        $this->user->save();

        //upload new file
        $newfile = UploadedFile::fake()->image('newpic.jpg');
        $response = $this->postJson("/api/users/{$this->user->id}/upload", [
            'uploadedFile' => $newfile,
        ]);

        $response->assertStatus(200);

        //Assert existing file is deleted
        Storage::disk('s3')->assertMissing($fakepath);

        //Assert new file is saved
        Storage::disk('s3')->assertExists("uploadedFileDir/{$this->user->id}/{$newfile->hashName()}");
    }

    /**
     * @param mixed $file
     */
    private function testUploadInvalidUser($file)
    {
        Storage::fake('s3');

        $response = $this->postJson('/api/users/999/upload', [
            'uploadedFile' => $file,
        ]);

        $response->assertStatus(404)->assertNotFound();
    }

    /**
     * @param mixed $file
     */
    private function testUploadImageFile($file)
    {
        Storage::fake('s3');

        $response = $this->postJson("/api/users/{$this->user->id}/upload", [
            'uploadedFile' => $file,
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'url' => "/storage/uploadedFileDir/{$this->user->id}/{$file->hashName()}",
            ]);

        // Assert the file was stored...
        Storage::disk('s3')->assertExists("uploadedFileDir/{$this->user->id}/{$file->hashName()}");
    }
}
