<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\CreatesApplication;
use App\Managers\DistrictManager;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class GetDistrictFromCoordsTest extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;
    use WithoutMiddleware;

    protected static $initialized = false;

    public function setUp(): void
    {
        parent::setUp();

        $dm = new DistrictManager();
        $dm->parseDataFromFile('district_test.kml');
        $dm->saveKMLData();
    }

    /**
     * Test get SG district from coord.
     */
    public function testGetSingaporeDistrictFromCoords()
    {
        //bukit panjang
        $lat = 1.3769083279008278;
        $lon = 103.7655357322067;

        $response = $this->getJson("/api/districts?lat={$lat}&lon={$lon}");

        $response->assertStatus(200)
            ->assertJson(['name' => 'Bukit Panjang / Mandai']);
    }

    /**
     * Test get non-SG district from coord.
     */
    public function testGetNonSingaporeDistrictFromCoords()
    {
        //somewhere in jeonju
        $lat = 35.816184019801256;
        $lon = 127.15720995268838;

        $response = $this->getJson("/api/districts?lat={$lat}&lon={$lon}");

        $response->assertNotFound();
    }

    /**
     * Test non numeric.
     */
    public function testGetDistrictWithNonNumericInputs()
    {
        $lat = 'abc';
        $lon = 'def';

        $response = $this->getJson("/api/districts?lat={$lat}&lon={$lon}");

        $response->assertStatus(422)
            ->assertJsonValidationErrors(['lat'])
            ->assertJsonValidationErrors(['lon']);
    }

    /**
     * Test no lat/lon.
     */
    public function testGetDistrictWithMissingInputs()
    {
        $response = $this->getJson('/api/districts');

        $response->assertStatus(422)
            ->assertJsonValidationErrors(['lat'])
            ->assertJsonValidationErrors(['lon']);
    }
}
