<?php

namespace Tests\Feature;

use App\User;
use App\Device;
use Tests\TestCase;
use Tests\CreatesApplication;
use App\Events\UserAssignedToDevice;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use App\Jobs\MailUserOnDeviceAssignment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class AddUserToDeviceTest extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;
    use WithoutMiddleware;

    public User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create(['email' => 'charlie@drive.sg']);
    }

    /**
     * Add valid user to device.
     */
    public function testAddValidUserToDevice()
    {
        //create 1 user to assign device to
        $test_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-12 00:00:00');

        //create 1 device with fixed device key and insert timing
        $device = Device::factory()->create([
            'device_key' => 'deviceWithUser',
            'device_value' => 'deviceWithUserValue',
            'created_at' => $test_dt,
            'updated_at' => $test_dt,
        ]);
        $deviceId = $device->id;

        // test the route
        // Queue::fake();
        Event::fake();
        $response = $this->putJson("/api/devices/{$deviceId}/add-user", ['email' => $this->user->email]);
        // Queue::assertPushed(MailUserOnDeviceAssignment::class);
        Event::assertDispatched(UserAssignedToDevice::class);

        $device = Device::find($deviceId);

        $response->assertStatus(200)
            ->assertJson($device->formatted)
            ->assertJsonFragment(['User' => $this->user->email]);
    }

    /**
     * Add invalid user to device.
     */
    public function testAddUserToNonExistantDevice()
    {
        // test the route
        $nonexistdeviceId = 999999;
        $response = $this->putJson("/api/devices/{$nonexistdeviceId}/add-user", ['email' => 'abc@gmail.com']);

        $response->assertStatus(400)
            ->assertJsonFragment(['message' => 'Device not found']);
    }

    /**
     * Add invalid user to device.
     */
    public function testAddInvalidUserToDevice()
    {
        //create 1 user to assign device to
        $test_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-12 00:00:00');

        //create 1 device with fixed device key and insert timing
        $device = Device::factory()->create([
            'device_key' => 'deviceWithUser',
            'device_value' => 'deviceWithUserValue',
            'created_at' => $test_dt,
            'updated_at' => $test_dt,
        ]);
        $deviceId = $device->id;

        // test the route
        $response = $this->putJson("/api/devices/{$deviceId}/add-user", ['email' => 'abc@gmail.com']);

        $device = Device::find($deviceId);

        $response->assertStatus(400)
            ->assertJsonFragment(['message' => 'User not found']);
    }
}
