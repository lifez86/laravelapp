<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class ExampleTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;

    /**
     * A basic test example.
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Route not found test.
     */
    public function testRouteNotFound()
    {
        // test the route
        $response = $this->getJson('/api/badroute');

        /*
         * Check the following
         * 1. http status
         * 2. number of records
         */
        $response->assertStatus(404)
            ->assertJson(['message' => 'Route does not exist']);
    }

    /**
     * Http method not allowed test.
     */
    public function testHttpMethodNotAllowed()
    {
        // test the route
        $response = $this->postJson('/api/devices/get_all_records', []);

        /*
         * Check the following
         * 1. http status
         * 2. number of records
         */
        $response->assertStatus(405)
            ->assertJson(['message' => 'Method not allowed']);
    }
}
