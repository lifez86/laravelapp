<?php

namespace Tests\Feature;

use App\Device;
use Tests\TestCase;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class GetOneDeviceTest extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;
    use WithoutMiddleware;

    /**
     * Get one device with string value.
     */
    public function testGetOneDeviceWithStringValue()
    {
        $test_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-12 00:00:00');

        //create 1 device with fixed device key and insert timing
        $device = Device::factory()->create([
            'device_key' => 'testdevice',
            'device_value' => 'testoutput',
            'created_at' => $test_dt,
            'updated_at' => $test_dt,
        ]);

        // test the route
        $response = $this->getJson('/api/devices/testdevice');

        $response->assertStatus(200);
        $this->assertIsString($response->getContent(), $device->device_value);
    }

    /**
     * Get one device with json value.
     */
    public function testGetOneDeviceWithJsonValue()
    {
        $test_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-12 00:00:00');

        //create 1 device with fixed device key and insert timing
        $device = Device::factory()->create([
            'device_key' => 'testdeviceJson',
            'device_value' => ['hello' => 'world'],
            'created_at' => $test_dt,
            'updated_at' => $test_dt,
        ]);

        // test the route
        $response = $this->getJson('/api/devices/testdeviceJson');

        $response
            ->assertStatus(200)
            ->assertExactJson($device->device_value);
    }

    /**
     * Returns latest device if there are duplicate entries.
     */
    public function testGetLatestDevice()
    {
        $test_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-12 00:00:00');
        $device1 = Device::factory()->create([
            'device_key' => 'device_latest',
            'device_value' => 'device_latest_value_1',
            'created_at' => $test_dt,
            'updated_at' => $test_dt,
        ]);

        $test2_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-13 00:00:00');
        $device1plus = Device::factory()->create([
            'device_key' => 'device_latest',
            'device_value' => 'device_latest_value_2',
            'created_at' => $test2_dt,
            'updated_at' => $test2_dt,
        ]);

        // test the route with filter
        $response = $this->getJson('/api/devices/device_latest');

        $response->assertStatus(200);
        $this->assertIsString($response->getContent(), $device1plus->device_value);
    }

    /**
     * Gets one device at given timestamp.
     */
    public function testGetOneDeviceAtGivenTimestampValue()
    {
        /**
         * Key record we are interested in.
         */
        $test_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-17 00:00:00');
        $test_dt_ts = $test_dt->getTimestamp();
        $device1 = Device::factory()->create([
            'device_key' => 'device1',
            'device_value' => 'device1_value1',
            'created_at' => $test_dt,
            'updated_at' => $test_dt,
        ]);

        $test2_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-18 00:00:00');
        $device2 = Device::factory()->create([
            'device_key' => 'device1',
            'device_value' => 'device1_value2',
            'created_at' => $test2_dt,
            'updated_at' => $test2_dt,
        ]);

        $test3_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-19 00:00:00');
        $device3 = Device::factory()->create([
            'device_key' => 'device1',
            'device_value' => 'device1_value3',
            'created_at' => $test3_dt,
            'updated_at' => $test3_dt,
        ]);

        // test the route with filter
        $response = $this->getJson('/api/devices/device1?timestamp='.$test_dt_ts);

        $response->assertStatus(200);
        $this->assertIsString($response->getContent(), $device1->device_value);
    }

    /**
     * Gets one device with empty timestamp.
     */
    public function testGetOneDeviceWithEmptyTimestampValue()
    {
        $test_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-12 00:00:00');
        $device1 = Device::factory()->create([
            'device_key' => 'device_empty_ts_1',
            'device_value' => 'device1_value',
            'created_at' => $test_dt,
            'updated_at' => $test_dt,
        ]);

        $test2_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-13 00:00:00');
        $device1plus = Device::factory()->create([
            'device_key' => 'device_empty_ts_1',
            'device_value' => 'device1_later_value',
            'created_at' => $test2_dt,
            'updated_at' => $test2_dt,
        ]);

        // test the route with filter
        $response = $this->getJson('/api/devices/device_empty_ts_1?timestamp=');

        $response->assertStatus(200);
        $this->assertIsString($response->getContent(), $device1plus->device_value);
    }

    /**
     * Get one device with key that does not exist.
     */
    public function testGetOneDeviceWithNonExistentKey()
    {
        // test the route
        $response = $this->getJson('/api/devices/nonexistkey');

        $response
            ->assertStatus(404)
            ->assertNotFound();
    }

    /**
     * Test device creation w/o value.
     */
    public function testGetOneDeviceWithInvalidTimestamp()
    {
        /**
         * Key record we are interested in.
         */
        $test_dt = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-04-12 00:00:00');
        $device1 = Device::factory()->create([
            'device_key' => 'device1',
            'device_value' => 'device1_value',
            'created_at' => $test_dt,
            'updated_at' => $test_dt,
        ]);

        // test the route with filter
        $wrongtimestamp = 'omgthisstring';
        $response = $this->getJson('/api/devices/device1?timestamp='.$wrongtimestamp);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['timestamp']);
    }
}
