<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Events\DeviceCreated;
use Tests\CreatesApplication;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class CreateDeviceTest extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;
    use WithoutMiddleware;

    /**
     * Test device creation with string value.
     */
    public function testCreateDeviceWithStringValue()
    {
        $deviceKey = 'deviceString';
        $deviceValue = 'device_string_value';
        $data = [$deviceKey => $deviceValue];

        Event::fake();
        $response = $this->postJson('/api/devices', $data);
        Event::assertDispatched(DeviceCreated::class);

        $response
            ->assertStatus(201)
            ->assertJson($data)
            ->assertJsonStructure([$deviceKey, 'Time']);
    }

    /**
     * Test device creation with json value.
     */
    public function testCreateDeviceWithJsonValue()
    {
        $deviceKey = 'deviceJson';
        $deviceValue = '{"test": "hello"}';
        $data = [$deviceKey => $deviceValue];

        Event::fake();
        $response = $this->postJson('/api/devices', $data);
        Event::assertDispatched(DeviceCreated::class);

        $response
            ->assertStatus(201)
            ->assertJson($data)
            ->assertJsonStructure([$deviceKey, 'Time']);

        //check API throttle limit header
        // $this->checkApiThrottleLimit($response);
    }

    // /**
    //  * Test device creation with empty value
    //  *
    //  * @return void
    //  */
    // public function testCreateDeviceWithEmptyValue()
    // {
    //     $deviceKey = 'deviceNoValue';
    //     $deviceValue = '';
    //     $data = [ $deviceKey => $deviceValue ];

    //     $response = $this->postJson('/api/devices', $data);

    //     $response
    //         ->assertStatus(201)
    //         ->assertJson($data)
    //         ->assertJsonStructure([$deviceKey, 'Time']);
    // }

    // /**
    //  * Test device creation with null value
    //  *
    //  * @return void
    //  */
    // public function testCreateDeviceWithNullValue()
    // {
    //     $deviceKey = 'deviceNullValue';
    //     $deviceValue = null;
    //     $data = [ $deviceKey => $deviceValue ];

    //     $response = $this->postJson('/api/devices', $data);

    //     $response
    //         ->assertStatus(201)
    //         ->assertJson($data)
    //         ->assertJsonStructure([$deviceKey, 'Time']);
    // }

    /**
     * Test device creation with non-string value.
     */
    public function testCreateDeviceWithNumericValue()
    {
        $deviceKey = 'deviceNonStringValue';
        $deviceValue = 9999;
        $data = [$deviceKey => $deviceValue];

        $response = $this->postJson('/api/devices', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['device_value']);
    }

    /**
     * Test device creation w/o key.
     */
    public function testCreateDeviceWithoutDeviceKey()
    {
        $deviceKey = '';
        $deviceValue = 'device_no_key';
        $data = [$deviceKey => $deviceValue];

        $response = $this->postJson('/api/devices', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['device_key']);
    }

    /**
     * Test device creation w/o more than 1 pair of key/value.
     */
    public function testCreateDeviceWithMultipleKeyValuePair()
    {
        $data = [
            'deviceMultiple1' => 'value1',
            'deviceMultiple2' => 'value2',
        ];

        $response = $this->postJson('/api/devices', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['data']);
    }

    /**
     * Test device creation with empty json.
     */
    public function testCreateDeviceWithEmptyJson()
    {
        $data = [];

        $response = $this->postJson('/api/devices', $data);

        $response
            ->assertStatus(400)
            ->assertJson(['message' => 'Please key in a valid key/value pair']);
    }

    /**
     * Test device creation with keys that are too short.
     */
    public function testCreateDeviceWithVeryShortDeviceKey()
    {
        $deviceKey = 'd1';
        $deviceValue = 'device_key_short_value';
        $data = [$deviceKey => $deviceValue];

        $response = $this->postJson('/api/devices', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['device_key']);
    }

    /**
     * Test device creation with keys that are too long.
     */
    public function testCreateDeviceWithVeryLongDeviceKey()
    {
        $i = 0;
        $longstr = '';
        while ($i < 256) {
            $longstr .= 'd';
            $i++;
        }
        $deviceKey = $longstr;
        $deviceValue = 'device_key_short_value';
        $data = [$deviceKey => $deviceValue];

        $response = $this->postJson('/api/devices', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['device_key']);
    }

    /**
     * Test device creation with device key containing non-alphanumeric keys.
     */
    public function testCreateDeviceWithNonAlphaNumericDeviceKey()
    {
        $deviceKey = '!@#$%^&*/';
        $deviceValue = 'device_non_alpha_numeric_value';
        $data = [$deviceKey => $deviceValue];

        $response = $this->postJson('/api/devices', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['device_key']);
    }
}
