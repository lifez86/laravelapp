<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class GetOneUserTest extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;
    use WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();

        $this->setUsersWithRoles();
    }

    /**
     * Test get one user.
     */
    public function testGetOneUser()
    {
        // test the route
        $user = User::take(1)->first();
        $userArray = $user->WithRoles;

        $response = $this->getJson('/api/users/'.$user->id);
        $response->assertStatus(200)
            ->assertExactJson($userArray);
    }

    /**
     * Test get invalid user.
     */
    public function testGetInvalidUser()
    {
        $response = $this->getJson('/api/users/999');
        $response->assertStatus(404)->assertNotFound();
    }
}
