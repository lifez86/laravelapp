<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class GetAllDevicesTest extends TestCase
{
    //db rollback after each each test
    use RefreshDatabase;
    use CreatesApplication;
    use WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();

        $this->createDevices();
    }

    /**
     * Get all devices.
     */
    public function testGetAllDevices()
    {
        // test the route
        $response = $this->getJson('/api/devices/get_all_records');

        $response->assertStatus(200)
            ->assertJsonCount(5);
    }
}
