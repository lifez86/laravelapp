<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * @internal
 * @coversNothing
 */
class GetAllUsersTest extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;
    use WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();

        $this->setUsersWithRoles();
    }

    /**
     * Test get all users.
     */
    public function testGetAllUsers()
    {
        // test the route
        $response = $this->getJson('/api/users');

        $response->assertStatus(200)
            ->assertJsonCount(5)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                    'created_at',
                    'updated_at',
                    'profilePic',
                    'roles',
                ],
            ]);
    }

    /**
     * Test get all users with ROLE_ADMIN filter.
     */
    public function testGetAllUsersWithRoleAdminFilter()
    {
        // test the route
        $response = $this->getJson('/api/users?role=ROLE_ADMIN');

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                    'created_at',
                    'updated_at',
                    'profilePic',
                    'roles',
                ],
            ]);

        $results = $response->getOriginalContent();
        foreach ($results as $user) {
            $this->assertContains('ROLE_ADMIN', $user['roles']);
        }
    }

    /**
     * Test get all users with non-existant ROLE_TESTER.
     */
    public function testGetAllUsersWithNonExistantRoleFilter()
    {
        // test the route
        $response = $this->getJson('/api/users?role=ROLE_TESTER');

        $response->assertStatus(200)
            ->assertJson([]);
    }
}
