<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\CreatesApplication;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Routing\Middleware\ThrottleRequests;

/**
 * @internal
 * @coversNothing
 */
class FakeApiTest extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;

    protected $admin;
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = $this->createAdmin();
        $this->user = $this->createUser();

        //only disable throttle middleware
        $this->withoutMiddleware(ThrottleRequests::class);
    }

    /**
     * Test route is protected.
     */
    public function testGetFakeApiWithoutAuthToken()
    {
        $response = $this->getJson('/api/fakeapi');

        $response->assertUnauthorized();
    }

    /**
     * Test route is protected.
     */
    public function testStoreToFakeApiWithoutAuthToken()
    {
        $response = $this->postJson('/api/fakeapi/store', [
            'title' => 'hello',
            'body' => 'world',
        ]);

        $response->assertUnauthorized();
    }

    /**
     * Test get data from endpoint.
     */
    public function testGetFakeApi()
    {
        Http::fake([
            'jsonplaceholder.typicode.com' => Http::response(['foo' => 'bar'], 200, []),
        ]);

        $response = $this->actingAs($this->admin, 'api')->getJson('/api/fakeapi');

        Http::assertSent(function (Request $request) {
            return $request->url() == 'https://jsonplaceholder.typicode.com/posts' && $request->method() == 'GET';
        });

        $response->assertStatus(200);
    }

    /**
     * Test store data in fake api.
     */
    public function testStoreInFakeApi()
    {
        Http::fake([
            'jsonplaceholder.typicode.com' => Http::response(['foo' => 'bar'], 200, []),
        ]);

        $response = $this->actingAs($this->admin, 'api')
            ->postJson('/api/fakeapi/store', [
                'title' => 'hello',
                'body' => 'world',
            ]);

        Http::assertSent(function (Request $request) {
            return $request->url() == 'https://jsonplaceholder.typicode.com/posts' && $request->method() == 'POST';
        });

        $response->assertStatus(200);
    }

    /**
     * Test data validation in store api.
     */
    public function testStoreInvalidDataInFakeApi()
    {
        $response = $this->actingAs($this->admin, 'api')
            ->postJson('/api/fakeapi/store', [
                'title' => 12345,
                'body' => 12345,
            ]);

        $response->assertJsonValidationErrors(['title', 'body']);
    }

    /**
     * Test only admin can access this endpoint.
     */
    public function testStoreInFakeApiWithNormalUser()
    {
        $response = $this->actingAs($this->user, 'api')
            ->postJson('/api/fakeapi/store', [
                'title' => 'hello',
                'body' => 'world',
            ]);

        $response->assertForbidden();
    }
}
