const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/district/district.js', 'public/js/district')
    .js('resources/js/district/checker.js', 'public/js/district')
    .js('resources/js/district/view.js', 'public/js/district')
    .js('resources/js/district/edit.js', 'public/js/district')
    .js('resources/js/trip.js', 'public/js')
    .js('resources/js/truck.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();
