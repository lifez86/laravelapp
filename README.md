# Laravelapp Readme

## API URL
[http://ec2-13-250-111-48.ap-southeast-1.compute.amazonaws.com](http://ec2-13-250-111-48.ap-southeast-1.compute.amazonaws.com)

## Routes
```
# Returns all device records
[GET] /api/devices/get_all_records

# Sample url
/api/devices/get_all_records

# Sample response
[
    {
        "device1": "value1",
        "Time": "15-04-2021 7:51pm"
    },
    {
        "device1": {
            "json": "value1"
        },
        "Time": "15-04-2021 7:52pm"
    },
    {
        "device2": "value2",
        "Time": "15-04-2021 7:55pm"
    },
    {
        "device2": {
            "key1": "value1",
            "key2": "value2"
        },
        "Time": "15-04-2021 7:55pm"
    }
]
```

```
# Returns latest device value (json blob/string) with specific key 
[GET] /api/devices/{key}

# Sample url
/api/devices/device1

# Sample response
{
    "json": "value1"
}

```

```
# Returns device value (json blob/string) with specific key inserted at the given timestamp
[GET] /api/devices/{key}?timestamp={timestamp}

# Sample url
/api/devices/device1?timestamp=1618516319 [15-04-2021 19:51 UTC]

# Sample response
"value1"
```

```
# Inserts a device with a key and string value
[POST] /api/devices

# Sample payload
{
    "device1": "value1"
}

# Sample response
{
    "device1": "value1",
    "Time": "15-04-2021 7:51pm"
}
```

```
# Inserts a device with a key and json value
[POST] /api/devices

# Sample payload
{
    "device1": {
        "json" : "value1"
    }
}

# Sample response
{
    "device1": {
        "json": "value1"
    },
    "Time": "15-04-2021 7:52pm"
}
```

## Testing

### To run all tests without coverage reports in html

```
# in linux
vendor/bin/phpunit --testdox --no-coverage

# in windows
".\vendor\bin\phpunit" --testdox --no-coverage
```

### To run tests with coverage reports in html

Run the command, the reports can be accessed in the /html-coverage folder within the project directory
```
# in linux
vendor/bin/phpunit --testdox

# in windows
".\vendor\bin\phpunit" --testdox
```

### In the event localhost is using settings from .env instead of .env.testing, run this command
```
php artisan config:clear
```

### Redis installation
```
# install redis extension for php
https://tecadmin.net/install-redis-ubuntu/
# install redis server for Ubuntu 20
https://linuxize.com/post/how-to-install-and-configure-redis-on-ubuntu-20-04/
```

### MySQL Geo Spatial Data
```
# Package doc
https://github.com/grimzy/laravel-mysql-spatial

# Singapore district dataset
https://data.gov.sg/dataset/sla-land-survey-district

# Tutorial on geo spatial data
https://medium.com/@brice_hartmann/getting-started-with-geospatial-data-in-laravel-94502dc74d55
```

### Docker useful commands

Useful links
```
https://maruan.medium.com/how-to-install-and-set-up-laravel-8-with-docker-compose-on-ubuntu-20-04-58149fed3e2e
https://medium.com/@sreejithezhakkad/how-i-set-up-laravel-in-docker-container-f80987559bc6
https://aregsar.com/blog/2020/laravel-app-with-mysql-in-docker/
https://aregsar.com/blog/2020/laravel-app-with-redis-in-docker/
```

Build image with specific .env file
```
docker-compose --env-file ./.env.docker build
```

Run docker with specific .env file
```
docker-compose --env-file ./.env.docker up -d
```

SSH web container
```
docker exec -it laravelapp-app bash
```

SSH db container
```
docker exec -it laravelapp-db mysql
```

Inspect docker container IP address
```
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' laravelapp-db
```

### Setup test db to run tests in docker

1. SSH db container
2. Create test db `CREATE DATABASE laravelapp_test;`
3. Create test db user `CREATE USER 'larauser'@'%' IDENTIFIED BY 'larauser123';`
4. Grant privileges to test db `GRANT ALL PRIVILEGES ON laravelapp_test.* TO 'larauser'@'%'; FLUSH PRIVILEGES;`
5. SSH web container
6. Run `php artisan migrate:fresh --env=testing`

### Notes related to file permissions

To ensure that you can clear cache properly without any permission issues, check the owner/group settings and listener.owner/group settings in php fpm. This ensures that project generated files like cache or uploaded file are created by this user/group.
```
# Open this file
sudo vi /etc/php/7.4/fpm/pool.d/laravelapp.conf

# Check settings in the file, ensure that it corresponds to the linux user used to deploy the project.

user = deployer
group = www-data

listen.owner = deployer
listen.group = www-data
```

To ensure that laravel horizon can run jobs which require file access, check the worker settings. Make sure the user corresponds to the owner of the created files in the project.
```
# Open the worker file
sudo vi /etc/supervisor/conf.d/laravel-worker.conf

# Update the user to the correct owner
user=deployer

# Refresh worker settings
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start laravel-worker:*
```

### Monitoring database queries

https://github.com/supliu/laravel-query-monitor
```
php artisan laravel-query-monitor
```
