<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\District.
 *
 * @property \Grimzy\LaravelMysqlSpatial\Types\MultiPolygon $geometry
 * @property int                                            $id
 * @property string                                         $name
 * @property string                                         $country
 * @property null|string                                    $city
 * @property null|string                                    $state
 * @property null|\Illuminate\Support\Carbon                $created_at
 * @property null|\Illuminate\Support\Carbon                $updated_at
 */
class District extends Model
{
    use HasFactory;
    use SpatialTrait;

    protected $fillable = [
        'name',
        'city',
        'country',
        'state',
        'geometry',
    ];

    protected $spatialFields = [
        'geometry',
    ];
}
