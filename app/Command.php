<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Command.
 *
 * @property int                        $id
 * @property string                     $desc
 * @property string                     $result
 * @property string                     $txnref
 * @property int                        $device_id
 * @property int                        $device_updated_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \App\Device                $device
 */
class Command extends BaseModel
{
    use HasFactory;

    public const DESC_LOCK = 'lock';
    public const DESC_UNLOCK = 'unlock';
    public const RESULT_OK = 'ok';
    public const RESULT_NOK = 'nok';

    //Attributes that are mass assignable
    protected $fillable = ['desc', 'result', 'txnref', 'device_id', 'device_updated_at', 'created_at', 'updated_at'];

    //Tablename
    protected $table = 'commands';

    /**
     * The primary key associated with the table.
     *
     * @var int
     */
    protected $primaryKey = 'id';

    public function device()
    {
        return $this->belongsTo('App\Device');
    }
}
