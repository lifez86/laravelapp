<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Trip.
 *
 * @property \Grimzy\LaravelMysqlSpatial\Types\LineString $route
 * @property int                                          $id
 * @property int                                          $truck_id
 * @property string                                       $start_date
 * @property string                                       $end_date
 * @property null|\Illuminate\Support\Carbon              $created_at
 * @property null|\Illuminate\Support\Carbon              $updated_at
 */
class Trip extends Model
{
    use HasFactory;
    use SpatialTrait;

    protected $fillable = [
        'truck_id',
        'start_date',
        'end_date',
        'route',
    ];

    protected $spatialFields = [
        'route',
    ];
}
