<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use App\Jobs\SendSlackNotificationJob;
use App\Jobs\MailUserOnDeviceAssignment;
use App\Jobs\SendDeviceDetailsToWebhook;

class DeviceEventSubscriber
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    public function handleUserAssignedToDevice($event)
    {
        //queue mailer job
        MailUserOnDeviceAssignment::dispatch($event->device);
    }

    public function handleDeviceCreated($event)
    {
        Log::info('Device created, ID is - '.$event->device->id);

        //queue slack notification job
        SendSlackNotificationJob::dispatch($event->device);
    }

    public function sendDeviceDetailsToWebhook($event)
    {
        //send device details to webhook
        SendDeviceDetailsToWebhook::dispatch($event->device);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\DeviceCreated',
            'App\Listeners\DeviceEventSubscriber@handleDeviceCreated'
        );

        $events->listen(
            'App\Events\UserAssignedToDevice',
            'App\Listeners\DeviceEventSubscriber@handleUserAssignedToDevice'
        );

        // $events->listen(
        //     'App\Events\UserAssignedToDevice',
        //     'App\Listeners\DeviceEventSubscriber@sendDeviceDetailsToWebhook'
        // );
    }
}
