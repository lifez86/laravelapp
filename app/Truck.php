<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Truck.
 *
 * @property int                             $id
 * @property string                          $model
 * @property null|\Illuminate\Support\Carbon $created_at
 * @property null|\Illuminate\Support\Carbon $updated_at
 * @property mixed                           $with_latest_location
 * @property null|\App\LatestLocation        $latestLocation
 * @property null|\App\CarState              $latest_carstate
 */
class Truck extends Model
{
    use HasFactory;

    //Attributes that are mass assignable
    protected $fillable = ['model'];

    /**
     * Get the latest location.
     */
    public function latestLocation()
    {
        return $this->hasOne(LatestLocation::class);
    }

    public function getWithLatestLocationAttribute()
    {
        $truckArray = $this->toArray();
        if (!empty($truckArray)) {
            $data = $this->latestLocation()->select('latlon', 'updated_at')->first();
            $truckArray['latestlocation'] = !empty($data) ? $data->toArray() : null;

            // might take too long to query
            // if(!empty($data)){
            //     $district = District::contains('geometry', $data->latlon)->pluck('name');
            //     $truckArray['district'] = !empty($district) ? $district[0] : null;
            // }
        }

        return $truckArray;
    }

    public function latest_carstate()
    {
        return $this->hasOne(CarState::class)->latestOfMany();
    }
}
