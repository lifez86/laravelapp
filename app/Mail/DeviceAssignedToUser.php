<?php

namespace App\Mail;

use App\Device;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DeviceAssignedToUser extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * The order instance.
     *
     * @var Device
     */
    public $device;

    /**
     * Create a new message instance.
     */
    public function __construct(Device $device)
    {
        $this->device = $device;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.device_assigned_to_user')
            ->with([
                'device_key' => $this->device->device_key,
                'device_value' => $this->device->device_value,
                'user_email' => $this->device->user->email,
            ]);
    }
}
