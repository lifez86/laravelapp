<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\CarState.
 *
 * @property \Grimzy\LaravelMysqlSpatial\Types\Point $latlon
 * @property int                                     $id
 * @property int                                     $truck_id
 * @property null|\Illuminate\Support\Carbon         $created_at
 * @property null|\Illuminate\Support\Carbon         $updated_at
 */
class CarState extends Model
{
    use HasFactory;
    use SpatialTrait;

    protected $fillable = [
        'truck_id',
        'latlon',
        'created_at',
        'updated_at',
    ];

    protected $spatialFields = [
        'latlon',
    ];
}
