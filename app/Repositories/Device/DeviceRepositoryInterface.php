<?php

namespace App\Repositories\Device;

interface DeviceRepositoryInterface
{
    public function all();

    public function find(int $id);

    public function findBy(array $searchkeys);

    public function create(array $data);
}
