<?php

namespace App\Repositories\Device;

use App\Device;

class DeviceRepository implements DeviceRepositoryInterface
{
    /**
     * useful ref:
     * https://stackoverflow.com/questions/33027047/what-is-the-difference-between-find-findorfail-first-firstorfail-get.
     *
     * implementing cache repository
     * https://adelf.tech/2019/read-eloquent-repositories
     */
    protected $model;

    public function __construct(Device $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function findBy($searchkeys)
    {
        return $this->model->where($searchkeys)
            ->latest()
            ->firstOrFail();
    }

    public function create($data)
    {
        return $this->model->create($data);
    }
}
