<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Device\DeviceRepository;
use App\Repositories\Device\DeviceRepositoryInterface;

class DeviceRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        $this->app->bind(
            DeviceRepositoryInterface::class,
            DeviceRepository::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
    }
}
