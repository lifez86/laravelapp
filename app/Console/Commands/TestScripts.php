<?php

namespace App\Console\Commands;

use App\Mail\CronFired;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class TestScripts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cmd:testScripts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run my test scripts';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //send email
        Mail::to('kchoongyee@gmail.com')->send(new CronFired());

        return 0;
    }
}
