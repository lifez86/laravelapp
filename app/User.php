<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User.
 *
 * @property int                                                                                                       $id
 * @property string                                                                                                    $name
 * @property string                                                                                                    $email
 * @property null|\Illuminate\Support\Carbon                                                                           $email_verified_at
 * @property string                                                                                                    $password
 * @property null|string                                                                                               $remember_token
 * @property null|\Illuminate\Support\Carbon                                                                           $created_at
 * @property null|\Illuminate\Support\Carbon                                                                           $updated_at
 * @property null|string                                                                                               $profilePic
 * @property \App\Device[]|\Illuminate\Database\Eloquent\Collection                                                    $devices
 * @property null|int                                                                                                  $devices_count
 * @property mixed                                                                                                     $with_roles
 * @property \Illuminate\Notifications\DatabaseNotification[]|\Illuminate\Notifications\DatabaseNotificationCollection $notifications
 * @property null|int                                                                                                  $notifications_count
 * @property \App\Role[]|\Illuminate\Database\Eloquent\Collection                                                      $roles
 * @property null|int                                                                                                  $roles_count
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasFactory;

    protected const SLACK_CHANNELS = [
        'general' => 'https://hooks.slack.com/services/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',
        'playground' => 'https://hooks.slack.com/services/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',
    ];

    protected $slack_url;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function devices()
    {
        return $this->hasMany('App\Device');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function getWithRolesAttribute()
    {
        $roles = $this->roles()->orderBy('name')->pluck('name');
        $userArray = $this->toArray();
        if (!empty($userArray)) {
            $userArray['roles'] = $roles->toArray();
        }

        return $userArray;
    }

    public function setSlackChannel($name)
    {
        if (isset(self::SLACK_CHANNELS[$name])) {
            $this->setSlackUrl(self::SLACK_CHANNELS[$name]);
        }

        return $this;
    }

    public function setSlackUrl($url)
    {
        $this->slack_url = $url;

        return $this;
    }

    public function routeNotificationForSlack($notification)
    {
        if ($this->slack_url === null) {
            return self::SLACK_CHANNELS['general'];
        }

        return $this->slack_url;
    }
}
