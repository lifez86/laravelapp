<?php

namespace App\Jobs;

use App\Truck;
use Throwable;
use App\CarState;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportCsv implements ShouldQueue
{
    use Batchable;
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        if ($this->batch()->cancelled()) {
            // Determine if the batch has been cancelled...

            return;
        }

        /**
         * THIS IS READING FROM CSV FILE AND STORING INTO DB.
         */
        // read each file and store the data into db
        $filename = $this->data['file'];
        $truck = Truck::findOrFail($this->data['truck']);

        $filehandler = fopen($filename, 'r');
        while (($data = fgetcsv($filehandler, 1000, ',')) !== false) {
            //handle timestamp
            $timezone = new \DateTimeZone('Asia/Singapore');
            $created_at = Carbon::createFromFormat('Y-m-d H:i:s', ltrim($data[2]), $timezone);
            $updated_at = Carbon::createFromFormat('Y-m-d H:i:s', ltrim($data[3]), $timezone);

            //create new carstate
            $point = new Point($data[0], $data[1]);
            CarState::create([
                'latlon' => $point,
                'truck_id' => $truck->id,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ]);
        }
        fclose($filehandler);
        Storage::delete($filename);
    }

    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc
        dump($exception->getMessage());
    }
}
