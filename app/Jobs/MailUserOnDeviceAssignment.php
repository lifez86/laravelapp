<?php

namespace App\Jobs;

use Throwable;
use App\Device;
use Illuminate\Bus\Queueable;
use App\Mail\DeviceAssignedToUser;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class MailUserOnDeviceAssignment implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $device;

    /**
     * Create a new job instance.
     *
     * @param Device $device
     */
    public function __construct(Device $device)
    {
        $this->device = $device;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        Mail::to('kchoongyee@gmail.com')->send(new DeviceAssignedToUser($this->device));
    }

    /**
     * Handle a job failure.
     *
     * @param \Throwable $exception
     */
    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc...
    }

    /**
    * Calculate the number of seconds to wait before retrying the job.
    *
    * @return array
    */
    public function backoff()
    {
        return [10, 20, 30];
    }
}
