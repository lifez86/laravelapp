<?php

namespace App\Jobs;

use App\Device;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Device\DeviceRepositoryInterface;

class ProcessDevice implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $deviceRepo;
    protected $deviceData;

    /**
     * Create a new job instance.
     *
     * @param mixed $deviceData
     */
    public function __construct($deviceData)
    {
        $this->deviceData = $deviceData;
    }

    /**
     * Execute the job.
     *
     * @return Device
     */
    public function handle(DeviceRepositoryInterface $deviceRepo)
    {
        $this->deviceRepo = $deviceRepo;

        return $this->deviceRepo->create($this->deviceData);
    }
}
