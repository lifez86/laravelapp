<?php

namespace App\Jobs;

use App\User;
use Throwable;
use App\Device;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\DeviceCreatedNotification;

class SendSlackNotificationJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $device;

    /**
     * Create a new job instance.
     *
     * @return Device $device
     */
    public function __construct(Device $device)
    {
        $this->device = $device;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $admin = User::where('email', 'admin@laravelapp.com')->first();
        if ($admin) {
            $admin->setSlackChannel('playground')->notify(new DeviceCreatedNotification($this->device));
        }
    }

    /**
     * Handle a job failure.
     *
     * @param \Throwable $exception
     */
    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc...
    }
}
