<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Device.
 *
 * @property int                                                     $id
 * @property string                                                  $device_key
 * @property null|array                                              $device_value
 * @property \Illuminate\Support\Carbon                              $created_at
 * @property \Illuminate\Support\Carbon                              $updated_at
 * @property null|int                                                $user_id
 * @property \App\Command[]|\Illuminate\Database\Eloquent\Collection $commands
 * @property null|int                                                $commands_count
 * @property mixed                                                   $formatted
 * @property mixed                                                   $time
 * @property mixed                                                   $timestamp
 * @property null|\App\User                                          $user
 */
class Device extends BaseModel
{
    use HasFactory;

    //Attributes that are mass assignable
    protected $fillable = ['device_key', 'device_value', 'created_at', 'updated_at'];

    //Hide some columns
    protected $hidden = ['id', 'created_at'];

    //Tablename
    protected $table = 'devices';

    /**
     * The primary key associated with the table.
     *
     * @var int
     */
    protected $primaryKey = 'id';

    //cast device_value into from json into array
    protected $casts = [
        'device_value' => 'array',
    ];

    protected $appends = [
        'Time',
        'Timestamp',
    ];

    /**
     * Outputs the device in this format
     * [ device_key => device_value ].
     */
    public function getFormattedAttribute()
    {
        $user = $this->user()->first();

        return [
            $this->device_key => $this->device_value,
            'Time' => $this->updated_at->format('d-m-Y g:ia'),
            'Timestamp' => $this->updated_at->getTimestamp(),
            'User' => (empty($user)) ? null : $user->email,
        ];
    }

    public function getTimestampAttribute()
    {
        return $this->updated_at->getTimestamp();
    }

    public function getTimeAttribute()
    {
        return $this->updated_at->format('d-m-Y g:ia');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function commands()
    {
        return $this->hasMany('App\Command');
    }

    public function latestCommand()
    {
        return $this->hasOne(Command::class)->latest();
    }
}
