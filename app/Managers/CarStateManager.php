<?php

namespace App\Managers;

use App\Truck;
use App\CarState;
use Illuminate\Support\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class CarStateManager
{
    public function saveDataFromFile(string $filename, Truck $truck)
    {
        $file = fopen(__DIR__.'../../../database/seeders/carstate/'.$filename, 'r');

        if ($file) {
            //getting column names
            $columns = fgetcsv($file, 1000, ',');

            while (($data = fgetcsv($file, 1000, ',')) !== false) {
                //e.g. "http://maps.google.com?q=1.38906%2C103.768723"
                // $latlon_str = substr($data[9], strpos($data[9], '=')+1);
                // $latlon = explode('%2C', $latlon_str);
                // $point = new Point($latlon[1], $latlon[0]);

                //handle timestamp
                $timezone = new \DateTimeZone('Asia/Singapore');
                $created_at = Carbon::createFromFormat('Y-m-d H:i:s', ltrim($data[2]), $timezone);
                $updated_at = Carbon::createFromFormat('Y-m-d H:i:s', ltrim($data[3]), $timezone);

                //create new carstate
                $point = new Point($data[0], $data[1]);
                CarState::create([
                    'latlon' => $point,
                    'truck_id' => $truck->id,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at,
                ]);
            }

            fclose($file);
        } else {
            throw new FileNotFoundException(sprintf('File not found: %s', $filename));
        }
    }
}
