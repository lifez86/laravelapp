<?php

namespace App\Managers;

use Illuminate\Support\Facades\Http;

class FakeApiManager
{
    protected $host;

    public function __construct()
    {
        $this->host = 'https://jsonplaceholder.typicode.com';
    }

    public function get()
    {
        $response = Http::get($this->host.'/posts');

        return $response->json();
    }

    public function store($data)
    {
        $response = Http::post($this->host.'/posts', $data);

        return $response->json();
    }
}
