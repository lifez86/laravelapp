<?php

namespace App\Managers;

use Exception;
use App\District;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\MultiPolygon;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class DistrictManager
{
    protected $data;

    public function parseDataFromFile(string $filename)
    {
        $file = file_get_contents(__DIR__.'../../../database/seeders/kml/'.$filename);

        if ($file) {
            $this->data = simplexml_load_string($file);
        } else {
            throw new FileNotFoundException(sprintf('File not found: %s', $filename));
        }
    }

    /**
     * Converts kml district number to names we know.
     */
    public function kmlToDistrictName(string $kml)
    {
        //TODO: poopulate names in https://data.gov.sg/dataset/sla-land-survey-district
        $districtNames = [
            'kml_51' => 'Tuas / Jurong West',
            'kml_39' => 'Jurong / Sentosa Island',
            'kml_64' => 'Jurong East',
            'kml_11' => 'Lim Chu Kang Training Area',
            'kml_55' => 'Jalan Bahar',
            'kml_49' => 'Lim Chu Kang',
            'kml_4' => 'Choa Chu Kang',
            'kml_48' => 'Marsling / Woodlands',
            'kml_15' => 'Bukit Panjang / Mandai',
            'kml_17' => 'Dairy Farm / Bukit Timah',
            'kml_3' => 'Holland',
        ];

        return empty($districtNames[$kml]) ? $kml : $districtNames[$kml];
    }

    /**
     * Saves KML data to database.
     */
    public function saveKMLData()
    {
        if (!empty($this->data)) {
            $districtData = $this->data->Document->Folder->Placemark;

            foreach ($districtData as $ds) {
                $districtName = $this->kmlToDistrictName($ds->name);
                $coordsString = $ds->Polygon->outerBoundaryIs->LinearRing->coordinates;
                $points = [];

                //e.g. 103.950157756439,1.35821281191433,0.0 103.950163501803,1.35828650813368,0.0
                $coords = explode(' ', $coordsString);
                foreach ($coords as $c) {
                    //only store latlon, coords given are in lon,lat,other
                    $latlon = explode(',', $c);
                    $point = new Point($latlon[1], $latlon[0]);
                    $points[] = $point;
                }

                $lineString = new LineString($points);
                $polygon = new Polygon([$lineString]);
                $multipolygon = new Multipolygon([$polygon]);

                // create district model
                $district = new District([
                    'name' => $districtName,
                    'country' => 'Singapore',
                    'geometry' => $multipolygon,
                ]);

                throw_unless(
                    $district->save(),
                    new Exception("Failed to save district '{$districtName}'")
                );
            }
        }
    }
}
