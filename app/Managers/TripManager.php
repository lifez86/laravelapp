<?php

namespace App\Managers;

use App\Truck;
use App\CarState;
use Illuminate\Support\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;

class TripManager
{
    /**
     * Returns line string object based on truck carstates for a given time period.
     */
    public function generateLineString(Truck $truck, Carbon $startDate, Carbon $endDate)
    {
        if (empty($truck) || empty($startDate) || empty($endDate)) {
            return false;
        }

        $carstates = CarState::query()
            ->where('truck_id', '=', $truck->id)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->get()
            ->pluck('latlon');

        return new LineString($carstates->toArray());
    }
}
