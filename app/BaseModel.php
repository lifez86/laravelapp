<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BaseModel.
 */
class BaseModel extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
}
