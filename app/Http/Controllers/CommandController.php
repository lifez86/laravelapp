<?php

namespace App\Http\Controllers;

use App\Device;
use App\Command;
use Illuminate\Http\Request;

class CommandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $request->all();
        $commands = Command::when(!empty($filters), function ($query) use ($filters) {
            if (!empty($filters['device'])) {
                $query->where('device_id', $filters['device']);
            }
        })->get();

        return $commands;
    }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param \Illuminate\Http\Request $request
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    // }

    /**
     * Display the specified resource.
     *
     * @param \App\Command $command
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Command $command)
    {
        return $command;
    }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param \App\Command $commands
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit(Command $commands)
    // {
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param \Illuminate\Http\Request $request
    //  * @param \App\Command             $commands
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, Command $command)
    // {
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param \App\Command $command
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy(Command $command)
    // {
    // }

    /**
     * Query on chunking results.
     *
     * @return \Illuminate\Http\Response
     */
    public function chunk(Request $request)
    {
        Command::where('desc', 'cmd1')
            ->chunkById(200, function ($cmd) {
                $cmd->each->update(['result' => 'nok']);
            }, $column = 'id');

        return Command::query()
            ->where('desc', '=', 'cmd1')
            ->get();
    }

    /**
     * Sub Select.
     *
     * @return \Illuminate\Http\Response
     */
    public function subselect(Request $request)
    {
        return Device::with('latestCommand')->get();
    }
}
