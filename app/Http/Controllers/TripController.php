<?php

namespace App\Http\Controllers;

use App\Trip;
use Illuminate\Http\Request;

class TripController extends Controller
{
    public function get(Request $request)
    {
        $filters = $request->all();
        $trucks = Trip::when(!empty($filters), function ($query) use ($filters) {
            if (!empty($filters['truck'])) {
                $query->where('truck_id', '=', $filters['truck']);
            }
        })->get();

        return $trucks;
    }

    public function show(Request $request, Trip $trip)
    {
        return $trip;
    }
}
