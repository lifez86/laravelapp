<?php

namespace App\Http\Controllers;

use App\User;
use App\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Http\Requests\DistrictRequest;
use App\Notifications\DistrictCreated;
use App\Http\Resources\DistrictResource;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\MultiPolygon;

class DistrictController extends Controller
{
    public function get(Request $request)
    {
        $request->validate([
            'lat' => 'required|numeric',
            'lon' => 'required|numeric',
        ]);

        //remember this query for 1 day
        // $seconds = 24 * 60 * 60;
        // $point = new Point($request->lat, $request->lon);
        // $redis_key = "district_".$request->lat."_".$request->lon;
        // $district = Cache::remember($redis_key, $seconds, function() use($point){
        //     return District::contains('geometry', $point)->firstOrFail();
        // });

        //if we are not using cache
        $point = new Point($request->lat, $request->lon);
        $district = new District();

        return $district->contains('geometry', $point)->firstOrFail();
        //publish to redis
        // Redis::publish('test-channel', json_encode([
        //     'district' => $district->name
        // ]));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'country' => 'required|string',
            'city' => 'required|string',
            'coords' => 'required|string',
        ]);

        $coordsDecoded = json_decode($request->coords);
        if ($coordsDecoded) {
            $points = [];
            foreach ($coordsDecoded as $latlon) {
                //only store latlon, coords given are in lon,lat,other
                $point = new Point($latlon[0], $latlon[1]);
                $points[] = $point;
            }

            //last point should be the same as 1st point
            $points[] = $points[0];

            $lineString = new LineString($points);
            $polygon = new Polygon([$lineString]);
            $multipolygon = new Multipolygon([$polygon]);

            // create district model
            $district = new District([
                'name' => $request->name,
                'country' => $request->country,
                'city' => $request->city,
                'geometry' => $multipolygon,
            ]);

            $district->save();

            // send notification
            $user = User::firstOrCreate(
                ['email' => 'kchoongyee@gmail.com'],
                ['name' => 'charlie kong', 'password' => bcrypt('12345')]
            );
            $user->notify(new DistrictCreated($district));

            return [
                'district' => $district->name,
            ];
        }
        abort(400, 'Unable to decode coordinates');
    }

    public function list()
    {
        return DistrictResource::collection(District::all());
    }

    public function update(District $district, DistrictRequest $request)
    {
        $validated = $request->validated();
        $district->update($validated);

        return new DistrictResource($district);
    }
}
