<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Managers\FakeApiManager;
use App\Http\Controllers\Controller;

class FakeApiWebController extends Controller
{
    protected $fakeApiMgr;

    public function __construct(FakeApiManager $fam)
    {
        $this->fakeApiMgr = $fam;
    }

    public function index(Request $request)
    {
        $posts = $this->fakeApiMgr->get();

        return view('fakeapi/index', ['posts' => $posts]);
    }

    public function store(Request $request)
    {
        /**
         * So, what if the incoming request parameters do not pass the given validation rules?
         * As mentioned previously, Laravel will automatically redirect the user back to their previous location.
         * In addition, all of the validation errors will automatically be flashed to the session.
         */
        $validated = $request->validate([
            'title' => 'required|string',
            'body' => 'required|string',
        ]);
        $post = $this->fakeApiMgr->store($validated);

        return view('fakeapi/store', ['post' => $post]);
    }
}
