<?php

namespace App\Http\Controllers\Web;

use App\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DistrictResource;

class DistrictWebController extends Controller
{
    public function index(Request $request)
    {
        return view('district/index', []);
    }

    public function listDistricts(Request $request)
    {
        if ($request->ajax()) {
            $data = DistrictResource::collection(District::all());

            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $viewURL = route('district.show', ['district' => $row['id']]);
                    $editURL = route('district.edit', ['district' => $row['id']]);

                    return '<a href="'.$viewURL.'" class="btn btn-primary btn-sm" target="_blank">View</a> <a href="'.$editURL.'" class="edit btn btn-success btn-sm" target="_blank">Edit</a>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function checker(Request $request)
    {
        return view('district/checker', []);
    }

    public function show(District $district)
    {
        $coords = $district->geometry[0][0]->toJson();

        return view('district/view', ['district' => $district, 'coords' => $coords]);
    }

    public function edit(District $district)
    {
        return view('district/edit', ['district' => $district]);
    }
}
