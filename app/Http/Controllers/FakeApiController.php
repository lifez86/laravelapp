<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Managers\FakeApiManager;
use Illuminate\Support\Facades\Gate;

class FakeApiController extends Controller
{
    protected $fakeApiMgr;

    public function __construct(FakeApiManager $fam)
    {
        $this->fakeApiMgr = $fam;
    }

    public function index(Request $request)
    {
        return $this->fakeApiMgr->get();
    }

    public function store(Request $request)
    {
        /*
         * So, what if the incoming request parameters do not pass the given validation rules?
         * As mentioned previously, Laravel will automatically redirect the user back to their previous location.
         * In addition, all of the validation errors will automatically be flashed to the session.
         */

        if (!Gate::allows('store-fakeapi', null)) {
            abort(403);
        }

        $validated = $request->validate([
            'title' => 'required|string',
            'body' => 'required|string',
        ]);

        return $this->fakeApiMgr->store($validated);
    }
}
