<?php

namespace App\Http\Controllers;

use App\CarState;
use App\LatestLocation;
use App\Http\Requests\StoreCarStateRequest;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class CarStateController extends Controller
{
    public function store(StoreCarStateRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $point = new Point($validated['lat'], $validated['lon']);
        $carState = CarState::create([
            'truck_id' => $validated['truck_id'],
            'latlon' => $point,
        ]);

        // Update latest location
        if ($carState) {
            $latestLocation = LatestLocation::updateOrCreate(
                ['truck_id' => $validated['truck_id']],
                ['latlon' => $point]
            );
        }

        return $carState;
    }
}
