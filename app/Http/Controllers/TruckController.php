<?php

namespace App\Http\Controllers;

use App\Truck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreTruckRequest;

class TruckController extends Controller
{
    public function get(Request $request)
    {
        /**
         * This method does a query with latestlocation table.
         */
        $filters = $request->all();
        $trucks = Truck::when(!empty($filters), function ($query) use ($filters) {
            if (!empty($filters['model'])) {
                $query->where(DB::raw('lower(model)'), 'like', '%'.strtolower($filters['model']).'%');
            } else {
                $query->get();
            }
        })->get();

        return $trucks->map(function ($truck) {
            return $truck->WithLatestLocation;
        });

        // This method does a inner join with car state table to get latest location
        // $filters = $request->all();
        // $trucks = Truck::when(!empty($filters), function ($query) use ($filters) {
        //     if (!empty($filters['model'])) {
        //         $query->where(DB::raw('lower(model)'), 'like', '%'.strtolower($filters['model']).'%');
        //     }else{
        //         $query->all();
        //     }
        // })->with('latest_carstate:car_states.truck_id,latlon,updated_at')->get();

        // return $trucks;
    }

    public function store(StoreTruckRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        return Truck::create($validated);
    }
}
