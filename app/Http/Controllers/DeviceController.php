<?php

namespace App\Http\Controllers;

use App\User;
use App\Device;
use App\Jobs\ProcessDevice;
use Illuminate\Http\Request;
use App\Events\DeviceCreated;
use App\Events\UserAssignedToDevice;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Device\DeviceRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DeviceController extends Controller
{
    protected $deviceRepo;

    public function __construct(DeviceRepositoryInterface $deviceRepo)
    {
        $this->deviceRepo = $deviceRepo;
    }

    public function index(Request $request)
    {
        $devices = Device::with(['user' => function ($query) {
            $query->select('id', 'email');
        }])->get();

        return $devices;
    }

    public function show(Request $request, $device_key)
    {
        //check for timestamp filter
        if (!empty($request->timestamp)) {
            $validated = $request->validate([
                'timestamp' => 'date_format:U',
            ]);

            $searchkeys = [
                ['param' => 'device_key', 'operator' => '=', 'value' => $device_key],
                ['param' => 'updated_at', 'operator' => '=', 'value' => $request->timestamp],
            ];
            $device = $this->deviceRepo->findBy($searchkeys);
        } else {
            $searchkeys = [
                ['param' => 'device_key', 'operator' => '=', 'value' => $device_key],
            ];
            $device = $this->deviceRepo->findBy($searchkeys);
        }

        return $device->device_value;
    }

    public function store(Request $request)
    {
        $data = $request->all();

        //no data found
        if (empty($data)) {
            throw new BadRequestHttpException('Please key in a valid key/value pair');
        }

        $device_keys = array_keys($data);
        $device_values = array_values($data);
        $device_data = [
            'data' => $data,
            'device_key' => $device_keys[0],
            'device_value' => is_array($device_values[0]) ? json_encode($device_values[0]) : $device_values[0],
        ];

        /**
         * Things to validate
         * 1. Nothing more than a key/value pair
         * 2. Key is of length 255, min length = 5, alphanumeric
         * 3. Value should be an string or json string.
         *
         * Perhaps use alpha_dash instead of alpha_num to allow dashes and underscore
         */
        $validated = Validator::make($device_data, [
            'data' => 'array|size:1',
            'device_key' => 'required|string|max:255|min:5|alpha_num',
            'device_value' => 'required|string',
        ])->validate();

        //create and return device
        $deviceObj = [
            'device_key' => $validated['device_key'],
            'device_value' => $validated['device_value'],
        ];

        // //dispatch w/o queueing
        // ProcessDevice::dispatchSync($deviceObj);

        // //dispatch with queueing
        // $thisdevice = ProcessDevice::dispatch($deviceObj);

        $thisdevice = Device::create($deviceObj);

        //dispatch event
        DeviceCreated::dispatch($thisdevice);

        return response()->json($thisdevice->formatted, 201);
    }

    public function addUser(Request $request, $device_id)
    {
        //find user and assign to device
        $device = Device::find($device_id);
        if (empty($device)) {
            throw new BadRequestHttpException('Device not found');
        }

        $email = $request->email;
        $user = User::where('email', $email)->first();
        if (empty($user)) {
            throw new BadRequestHttpException('User not found');
        }
        $device->user()->associate($user);
        $device->save();

        //dispatch event
        UserAssignedToDevice::dispatch($device);

        return response()->json($device->formatted, 200);
    }
}
