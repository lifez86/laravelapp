<?php

namespace App\Http\Controllers;

use App\Jobs\ImportCsv;
use App\Jobs\ImportData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BatchController extends Controller
{
    /**
     * Chunk the data from huge csv file and batch process.
     */
    public function upload(Request $request)
    {
        if (!empty($request->csvfile)) {
            //remove header
            $data = file($request->csvfile);
            unset($data[0]);

            //chunking file
            $chunks = array_chunk($data, 500);

            //convert chunks into smaller csv file
            $batch = Bus::batch([])
                ->name('Batch insert carstates from huge csv file')
                ->allowFailures()
                ->dispatch();

            foreach ($chunks as $key => $chunk) {
                $data = ['chunk' => $chunk, 'truck' => 2];
                $batch->add(new ImportData($data));
            }

            return $batch;
        }
        abort(400, 'Please upload a csv file');
    }

    /**
     * Chunk the data from original csv file into smaller csv files, then queue insertion.
     */
    public function uploadSplit(Request $request)
    {
        if (!empty($request->csvfile)) {
            // original filename
            $oFilename = pathinfo($request->csvfile->getClientOriginalName(), PATHINFO_FILENAME);

            //remove header
            $data = file($request->csvfile);
            unset($data[0]);

            //chunking file
            $chunks = array_chunk($data, 1000);

            //check if temp dir exists
            // $path = public_path('temp');
            // File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

            $fileDir = [];
            foreach ($chunks as $key => $chunk) {
                $name = "temp/{$oFilename}_{$key}.csv";
                Storage::disk('public')->put($name, $chunk);
                //file_put_contents($path.$name, $chunk);
                $fileDir[] = storage_path($name);
            }

            return $fileDir;
        }
        abort(400, 'Please upload a csv file');
    }

    /**
     * Store chunked csv data in files to database.
     */
    public function store(Request $request)
    {
        $path = storage_path('app/public/temp');
        $files = glob("{$path}/*.csv");

        //convert chunks into smaller csv file
        $batch = Bus::batch([])
            ->name('Batch insert carstates from smaller csv files')
            ->allowFailures()
            ->dispatch();

        foreach ($files as $file) {
            $data = ['file' => $file, 'truck' => 2];
            $batch->add(new ImportCsv($data));
        }

        return $batch;
    }

    /**
     * Returns batch info from batch id.
     *
     * @param mixed $id
     */
    public function get(Request $request, $id)
    {
        return Bus::findBatch($id);
    }
}
