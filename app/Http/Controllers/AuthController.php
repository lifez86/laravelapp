<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\RoleUser;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        //get token ttl in minutes
        $ttl = env('JWT_TTL', 3600);

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string',
            'roles' => 'array',
        ]);

        if (!User::where('email', $request->email)->exists()) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            //set roles if provided
            if (!empty($request->roles)) {
                $roles = Role::whereIn('name', $request->roles)->get();
                foreach ($roles as $role) {
                    RoleUser::create([
                        'user_id' => $user->id,
                        'role_id' => $role->id,
                    ]);
                }
            }

            $token = auth()->setTTL($ttl)->login($user);

            return $this->respondWithToken($token);
        }

        throw new BadRequestHttpException('User exists');
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token)
    {
        //expiry timestamp in seconds
        $expiry_ts = auth()->factory()->getTTL() * 60;

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expiry_ts,
        ]);
    }
}
