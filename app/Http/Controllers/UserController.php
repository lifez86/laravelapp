<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $filter_role = $request->role;
        if (!empty($filter_role)) {
            $role = Role::where('name', '=', $filter_role)->first();
            if (empty($role)) {
                return [];
            }
            $users = $role->users()->get();

            return $users->map(function ($user) {
                return $user->makeHidden('pivot')->WithRoles;
            });
        }

        return User::all()->map(function ($user) {
            return $user->WithRoles;
        });
    }

    public function show($userId)
    {
        return User::findOrFail($userId)->WithRoles;
    }

    public function uploadFile(Request $request, $userId)
    {
        $user = User::find($userId);
        if (empty($user)) {
            throw new BadRequestHttpException('User not found');
        }

        //check file requirements
        $validated = $request->validate([
            'uploadedFile' => 'required|mimes:jpeg,png|max:10240',
        ]);

        //check if file exists
        $currFile = $user->profilePic;
        $exists = Storage::disk('s3')->exists($currFile);
        if ($exists) {
            Storage::disk('s3')->delete($currFile);
        }

        //store image in s3
        $path = $validated['uploadedFile']->storePublicly(
            'uploadedFileDir/'.$user->id,
            's3'
        );

        //save s3 path in db
        $user->profilePic = $path;
        $user->save();

        //return public url
        $url = Storage::cloud()->url($user->profilePic);

        return ['url' => $url];
    }
}
