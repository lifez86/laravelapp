<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Role.
 *
 * @property int                                                  $id
 * @property string                                               $name
 * @property \Illuminate\Support\Carbon                           $created_at
 * @property \Illuminate\Support\Carbon                           $updated_at
 * @property \App\User[]|\Illuminate\Database\Eloquent\Collection $users
 * @property null|int                                             $users_count
 */
class Role extends BaseModel
{
    use HasFactory;

    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_DEV = 'ROLE_DEV';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withTimestamps();
    }
}
