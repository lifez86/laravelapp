<?php

namespace App\Notifications;

use App\Device;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\SlackMessage;

class DeviceCreatedNotification extends Notification
{
    use Queueable;

    protected Device $device;

    /**
     * Create a new notification instance.
     */
    public function __construct(Device $device)
    {
        $this->device = $device;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\SlackMessage
     */
    public function toSlack($notifiable)
    {
        $charlie = '<@U02FY68GJG5>';
        $devicemsg = "{$charlie} Device created - {$this->device->device_key} | {$this->device->device_value}";

        return (new SlackMessage())
            ->from('Ghost', ':ghost:')
            ->to('#general')
            ->content($devicemsg);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
        ];
    }
}
