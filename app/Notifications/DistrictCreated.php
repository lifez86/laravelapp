<?php

namespace App\Notifications;

use App\District;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Config;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DistrictCreated extends Notification implements ShouldQueue
{
    use Queueable;

    public $connection;
    public $afterCommit;
    public $district;

    /**
     * Create a new notification instance.
     */
    public function __construct(District $district)
    {
        $this->district = $district;
        $this->connection = 'database';
        $this->afterCommit = true;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Determine which queues should be used for each notification channel.
     *
     * @return array
     */
    public function viaQueues()
    {
        return [
            'mail' => 'mail-queue',
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $from = Config::get('mail.mailers.smtp.username');

        return (new MailMessage())
            ->from($from, 'Charlie Kong')
            ->subject('District Created')
            ->view('emails.district_created', ['district' => $this->district]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
        ];
    }
}
