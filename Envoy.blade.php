# Reference: https://gist.github.com/gvsrepins/80527aa61d4e49f68302

@setup
    $app_dir = '/var/www/html/laravelapp';
    $repository = 'git@gitlab.com:lifez86/laravelapp.git';
    $releases_dir = $app_dir . '/releases';
    $current_dir = $app_dir . '/current';   
    $release = date('d-m-Y-His');
    $new_release_dir = $releases_dir .'/'. $release;
    $user = "deployer";
    $host = "ec2-13-250-111-48.ap-southeast-1.compute.amazonaws.com";

    function logMessage($message) {
        return "echo '\033[32m" .$message. "\033[0m';\n";
    }

    $writable = [
        'storage'
    ];

    $shared = [
        'storage' => 'd'
    ];
@endsetup

@servers(['prod' => $user.'@'.$host])

@story('deploy')
    clone_repository
    run_composer
    run_npm
    laravel_horizon
    symlinks
    migrate_release
    cache
    clean_old_releases
    finish
@endstory

@story('rollback')
	deployment_rollback
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -o -v
@endtask

@task('laravel_horizon')
    {{ logMessage("Publishing Horizon assets") }}
    php {{ $new_release_dir }}/artisan horizon:publish --ansi
@endtask

@task('cache')
    {{ logMessage("Building cache") }}

    php {{ $current_dir }}/artisan route:clear

    php {{ $current_dir }}/artisan route:cache

    php {{ $current_dir }}/artisan cache:clear

    php {{ $current_dir }}/artisan config:cache

    php {{ $current_dir }}/artisan view:cache

    php {{ $current_dir }}/artisan event:clear

    php {{ $current_dir }}/artisan event:cache
@endtask

@task('clean_old_releases')
    # Delete all but the 5 most recent releases
    {{ logMessage("Cleaning old releases") }}
    cd {{ $releases_dir }}
    ls -dt {{ $releases_dir }}/* | tail -n +6 | xargs -d "\n" rm -rf;
@endtask

@task('migrate_release', ['on' => 'prod', 'confirm' => false])
    {{ logMessage("Running migrations") }}

    php {{ $new_release_dir }}/artisan migrate --force
@endtask

@task('migrate_rollback', ['on' => 'prod', 'confirm' => true])
    {{ logMessage("Rolling back migrations") }}

    php {{ $current_dir }}/artisan migrate:rollback --force
@endtask

@task('migrate_status', ['on' => 'prod'])
    php {{ $current_dir }}/artisan migrate:status
@endtask

@task('permissions')
    @foreach($writable as $item)
        # Using 777 until there is a better solution...
        chmod -R 777 {{ $new_release_dir }}/{{ $item }}
        #chmod -R 755 {{ $new_release_dir }}/{{ $item }}
        #chmod -R g+s {{ $new_release_dir }}/{{ $item }}
        chgrp -R www-data {{ $new_release_dir }}/{{ $item }}
        echo "Permissions have been set for {{ $new_release_dir }}/{{ $item }}"
    @endforeach
@endtask

@task('symlinks')
    echo 'update .env file'
    cp {{ $new_release_dir }}/.env.web1 {{ $app_dir }}/.env

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    [ -d {{ $app_dir }}/shared ] || mkdir -p {{ $app_dir }}/shared;

    @foreach($shared as $item => $type)
        #// if the item passed exists in the shared folder and in the release folder then
        #// remove it from the release folder;
        #// or if the item passed not existis in the shared folder and existis in the release folder then
        #// move it to the shared folder

        if ( [ -{{ $type }} '{{ $app_dir }}/shared/{{ $item }}' ] && [ -{{ $type }} '{{ $new_release_dir }}/{{ $item }}' ] );
        then
        rm -rf {{ $new_release_dir }}/{{ $item }};
        echo "rm -rf {{ $new_release_dir }}/{{ $item }}";
        elif ( [ ! -{{ $type }} '{{ $app_dir }}/shared/{{ $item }}' ]  && [ -{{ $type }} '{{ $new_release_dir }}/{{ $item }}' ] );
        then
        mv {{ $new_release_dir }}/{{ $item }} {{ $app_dir }}/shared/{{ $item }};
        echo "mv {{ $new_release_dir }}/{{ $item }} {{ $app_dir }}/shared/{{ $item }}";
        fi

        ln -nfs {{ $app_dir }}/shared/{{ $item }} {{ $new_release_dir }}/{{ $item }}
        echo "Symlink has been set for {{ $new_release_dir }}/{{ $item }}"
    @endforeach

    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current;
    chgrp -h www-data {{ $app_dir }}/current;
    echo "All symlinks have been set"
@endtask

@task('finish')
    echo "http://{{ $host }} is ready to run"
@endtask

@task('deployment_rollback')
    cd {{ $releases_dir }}
	ln -nfs {{ $releases_dir }}/$(find . -maxdepth 1 | sort | tail -n 2 | head -n 1) {{ $app_dir }}/current
	echo "Rolled back to {{ $releases_dir }}/$(find . -maxdepth 1 | sort | tail -n 2 | head -n 1)"
@endtask

@task('run_npm')
    cd {{ $new_release_dir }}
    npm install && npm run prod
@endtask

