window.axios = require("axios");

let map;
let center = { lat: 1.3690998598639286, lng: 103.80869389344295 };

window.initMap = () => {
    let mapOptions = {
        zoom: 12,
        center: center,
        mapTypeId: google.maps.MapTypeId.RoadMap,
    };

    map = new google.maps.Map(document.getElementById("map"), mapOptions);

    getTripData(1);
}

const getTripData = (tripId) => {
    axios.get(`/api/trips/${tripId}`).then(
        (response) => {
            drawRoute(response.data.route.coordinates);
        },
        (error) => {
            console.log(error);
        }
    );
}

const drawRoute = (coords) => {
    let myCoords = [];
    for (let i = 0; i < coords.length; i++) {
        myCoords.push({ lat: coords[i][1], lng: coords[i][0] });

        //create marker for start / end
        if (i === 0 || i === coords.length - 1) {
            i === 0 ? (tripLabelText = "S") : (tripLabelText = "E");
            let myLatlng = new google.maps.LatLng(coords[i][1], coords[i][0]);
            let tripMarker = new google.maps.Marker({
                position: myLatlng,
                label: {
                    text: tripLabelText,
                },
            });
            tripMarker.setMap(map);
        }
    }

    const route = new google.maps.Polyline({
        path: myCoords,
        geodesic: true,
        strokeColor: "#FFAE03",
        strokeOpacity: 1.0,
        strokeWeight: 2,
    });

    route.setMap(map);
}