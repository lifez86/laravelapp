window.axios = require("axios");
window.$ = window.jQuery = require('jquery');

const getDistricts = () => {
    axios.get(`/api/districts/list`).then(
        (response) => {
            displayData(response.data);
        },
        (error) => {
            console.log(error);
        }
    );
};

const displayData = (data) => {
    var table = $(".yajra-datatable").DataTable({
        processing: true,
        serverSide: true,
        ajax: "/districts/ajax-list",
        columns: [
            { data: "id", name: "id"},
            { data: "name", name: "name" },
            { data: "city", name: "city" },
            { data: "country", name: "country" },
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ],
    });
};

$(function() {
    displayData();
});
