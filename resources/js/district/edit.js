window.axios = require('axios');

const displayNotification = (elementId, type, message) => {
    let outcomeDiv = document.getElementById(elementId);
    if (type === "success") {
        outcomeDiv.innerHTML = `<div class="alert alert-success" role="alert">${message}</div>`;
    } else if (type === "danger") {
        outcomeDiv.innerHTML = `<div class="alert alert-danger" role="alert">${message}</div>`;
    }

    return outcomeDiv;
}

window.updateDistrict = (e) => {
    toggleLoading("submitBtn", "show");
    e.preventDefault();
    let formData = new FormData(e.target);
    
    // update takes in json object
    const districtObj = {};
    formData.forEach((value, key) => {
        if(value) {
            districtObj[key] = value;
        }
    });

    axios.put(`/api/districts/${districtId}`, districtObj).then(
        (response) => {
            toggleLoading("submitBtn", "hide");
            displayNotification(
                "outcome",
                "success",
                `District updated!`
            );
        },
        (error) => {
            // console.log(error);
            toggleLoading("submitBtn", "hide");
            displayNotification("outcome", "danger", `${error}`);
        }
    );
}

const toggleLoading = (btnId, state) => {
    let submitBtn = document.getElementById(btnId);
    if (state === "show") {
        submitBtn.innerHTML = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`;
    } else {
        submitBtn.innerHTML = "Submit";
    }
}