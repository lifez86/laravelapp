window.axios = require('axios');

let map;
let marker;
let myPolygon;
let center = { lat: 1.3690998598639286, lng: 103.80869389344295 };
let polygonCoords = [
    [1.383556, 103.755849],
    [1.353696, 103.84642],
    [1.301674, 103.815106],
];

window.initMap = () => {
    let mapOptions = {
        zoom: 12,
        center: center,
        mapTypeId: google.maps.MapTypeId.RoadMap,
    };

    map = new google.maps.Map(document.getElementById("map"), mapOptions);

    // Construct the polygon.
    drawPolygon(polygonCoords, false);
}

const drawPolygon = (coords, flipcoords) => {
    let myCoords = [];
    let unflippedCoords = [];
    if (coords.length > 0) {
        for (let i = 0; i < coords.length; i++) {
            if (flipcoords) {
                //from server
                const pt = new google.maps.LatLng(coords[i][1], coords[i][0]);
                myCoords.push(pt);

                //unflip the coords from server
                unflippedCoords.push([coords[i][1], coords[i][0]]);
            } else {
                //from default input
                const pt = new google.maps.LatLng(coords[i][0], coords[i][1]);
                myCoords.push(pt);
            }
        }

        //reset polygons
        if (myPolygon) myPolygon.setMap(null);

        //Construct the polygon.
        myPolygon = new google.maps.Polygon({
            paths: myCoords,
            draggable: true, // turn off if it gets annoying
            editable: true,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
        });

        //display polygon on map
        myPolygon.setMap(map);

        //add listeners
        google.maps.event.addListener(
            myPolygon.getPath(),
            "insert_at",
            () => { showArrays(myPolygon.getPath()) }
        );
        google.maps.event.addListener(
            myPolygon.getPath(),
            "set_at",
            () => { showArrays(myPolygon.getPath()) }
        );
        google.maps.event.addListener(
            myPolygon.getPath(),
            "remove_at",
            () => { showArrays(myPolygon.getPath()) }
        );

        //get unflipped coords if any
        if (unflippedCoords.length > 0) polygonCoords = unflippedCoords;

        //set default value for form
        let coordsInput = document.getElementById("coords");
        coordsInput.innerHTML = JSON.stringify(polygonCoords);
    }
}

const showArrays = (path) => {
    // Since this polygon has only one path, we can call getPath() to return the
    // MVCArray of LatLngs.
    const vertices = path;

    //reset coords
    polygonCoords = [];

    // Iterate over the vertices.
    for (let i = 0; i < vertices.getLength(); i++) {
        const xy = vertices.getAt(i);
        polygonCoords.push([xy.lat(), xy.lng()]);
    }

    // Replace the info window's content and position.
    let coordsInput = document.getElementById("coords");
    coordsInput.innerHTML = JSON.stringify(polygonCoords);
}

window.checkDistrict = (e) => {
    toggleLoading("checkBtn", "show");
    e.preventDefault();
    let formData = new FormData(e.target);

    // Display the key/value pairs
    let keyvalue;
    for (var pair of formData.entries()) {
        let latlon = pair[1].split(",");
        keyvalue = {
            lat: latlon[0],
            lon: latlon[1],
        };
    }

    // Display where the user is searching
    if (keyvalue.lat && keyvalue.lon) {
        displayLocationMarker(keyvalue.lat, keyvalue.lon);
    }

    axios.get("/api/districts", { params: keyvalue }).then(
        (response) => {
            toggleLoading("checkBtn", "hide");
            displayNotification(
                "district-results",
                "success",
                `District name is: ${response.data.name}`
            );
            drawPolygon(response.data.geometry.coordinates[0][0], true);
        },
        (error) => {
            // console.log(error);
            toggleLoading("checkBtn", "hide");
            displayNotification("district-results", "danger", `${error}`);
        }
    );
}

const displayNotification = (elementId, type, message) => {
    let outcomeDiv = document.getElementById(elementId);
    if (type === "success") {
        outcomeDiv.innerHTML = `<div class="alert alert-success" role="alert">${message}</div>`;
    } else if (type === "danger") {
        outcomeDiv.innerHTML = `<div class="alert alert-danger" role="alert">${message}</div>`;
    }

    return outcomeDiv;
}

window.createDistrict = (e) => {
    toggleLoading("submitBtn", "show");
    e.preventDefault();
    let formData = new FormData(e.target);

    // Display the key/value pairs
    // for(var pair of formData.entries()) {
    // console.log(pair[0]+ ', '+ pair[1]);
    // }

    axios.post("/api/districts", formData).then(
        (response) => {
            toggleLoading("submitBtn", "hide");
            displayNotification(
                "outcome",
                "success",
                `District created: ${response.data.district}`
            );
        },
        (error) => {
            // console.log(error);
            toggleLoading("submitBtn", "hide");
            displayNotification("outcome", "danger", `${error}`);
        }
    );
}

const toggleLoading = (btnId, state) => {
    let submitBtn = document.getElementById(btnId);
    if (state === "show") {
        submitBtn.innerHTML = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`;
    } else {
        submitBtn.innerHTML = "Submit";
    }
}

window.resetPolygon = (event) => {
    event.preventDefault();
    polygonCoords = [
        [1.383556, 103.755849],
        [1.353696, 103.84642],
        [1.301674, 103.815106],
    ];
    drawPolygon(polygonCoords, false);
}

const displayLocationMarker = (lat, lon) => {
    if (map) {
        //clear marker on map
        if (marker) {
            marker.setMap(null);
        }

        //set marker on map
        let myLatlng = new google.maps.LatLng(lat, lon);
        marker = new google.maps.Marker({
            position: myLatlng,
        });
        marker.setMap(map);
        map.setZoom(14);
        map.setCenter(myLatlng);
    }
}
