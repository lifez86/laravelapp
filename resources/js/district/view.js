window.axios = require('axios');

let map;
let myPolygon;
let center = { lat: 1.3690998598639286, lng: 103.80869389344295 };

window.initMap = () => {
    let mapOptions = {
        zoom: 11,
        center: center,
        mapTypeId: google.maps.MapTypeId.RoadMap,
    };

    map = new google.maps.Map(document.getElementById("map"), mapOptions);

    // Construct the polygon.
    drawPolygon(coordsFromView, true);
}

const drawPolygon = (coords, flipcoords) => {
    let myCoords = [];
    let unflippedCoords = [];
    if (coords.length > 0) {
        for (let i = 0; i < coords.length; i++) {
            if (flipcoords) {
                //from server
                const pt = new google.maps.LatLng(coords[i][1], coords[i][0]);
                myCoords.push(pt);

                //unflip the coords from server
                unflippedCoords.push([coords[i][1], coords[i][0]]);
            } else {
                //from default input
                const pt = new google.maps.LatLng(coords[i][0], coords[i][1]);
                myCoords.push(pt);
            }
        }

        //reset polygons
        if (myPolygon) myPolygon.setMap(null);

        //Construct the polygon.
        myPolygon = new google.maps.Polygon({
            paths: myCoords,
            draggable: true, // turn off if it gets annoying
            editable: true,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
        });

        //display polygon on map
        myPolygon.setMap(map);
    }
}
