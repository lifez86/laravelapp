window.axios = require("axios");

let map;
let center = { lat: 1.3690998598639286, lng: 103.80869389344295 };

window.initMap = () => {
    let mapOptions = {
        zoom: 12,
        center: center,
        mapTypeId: google.maps.MapTypeId.RoadMap,
    };

    map = new google.maps.Map(document.getElementById("map"), mapOptions);

    getTruckLocations();
}

const getTruckLocations = () => {
    axios.get(`/api/trucks`).then(
        (response) => {
            let truckDiv = document.getElementById("truck-list");
            truckDiv.innerHTML = "";
            truckList = "";
            response.data.map((truck) => {
                drawMarkers(truck);
                truckList += `<li>${truck["id"]} | ${truck["model"]} | ${truck["latestlocation"]["updated_at"]}</li>`;
            });
            truckDiv.innerHTML = `<ul>${truckList}</ul>`;
        },
        (error) => {
            console.log(error);
        }
    );
}

const drawMarkers = (truck) => {
    let latlon = truck["latestlocation"]["latlon"]["coordinates"];
    let truckId = String(truck["id"]);
    let myLatlng = new google.maps.LatLng(latlon[1], latlon[0]);
    let tripMarker = new google.maps.Marker({
        position: myLatlng,
        label: {
            text: truckId,
        },
    });
    tripMarker.setMap(map);
}
