@extends('layouts.default')

@section('title')
Districts
@endsection

@section('pageScripts')
<script>
    const districtId = "{{ $district->id }}";
</script>
<script src="{{ URL::asset('/js/district/edit.js') }}"></script>
@endsection

@section('content')
<div class="row justify-content-center pt-4">
    <div class="col col-lg-8">
        <h1>Edit District</h1>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div id="outcome"></div>
                        <form name="editDistrictForm" id="editDistrictForm" onsubmit="updateDistrict(event)">
                            <div class="form-group">
                                <label for="name">District Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $district->name }}" required>
                            </div>
                            <div class="form-group">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" id="country" name="country" value="{{ $district->country }}" required>
                            </div>
                            <div class="form-group">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" name="city" value="{{ $district->city }}">
                            </div>
                            <button type="submit" class="btn btn-primary" id="submitBtn">
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
