@extends('layouts.default')

@section('title')
Districts
@endsection

@section('pageScripts')
<script src="{{ URL::asset('/js/district/checker.js') }}"></script>
@endsection

@section('content')
<div class="row justify-content-center pt-4">
    <div class="col col-lg-8">
        <h1>District Checker / Generator</h1>
        <div class="card">
            <div class="card-img-top" id="map"></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <h5 class="card-title">Check District</h5>
                        <div id="district-results"></div>
                        <form class="form-inline" name="checkDistrictForm" id="checkDistrictForm" onsubmit="checkDistrict(event)">
                            <div class="form-group mb-2 mr-sm-2">
                                <input type="text" class="form-control" id="latlon" name="latlon" placeholder="latlon" value="1.3793107740494912, 103.7633323774796">
                            </div>
                            <button type="submit" class="btn btn-primary mb-2" id="checkBtn">
                                Submit
                            </button>
                        </form>
                    </div>
                    <div class="col-12 col-lg-6">
                        <h5 class="card-title">Create District</h5>
                        <div id="outcome"></div>
                        <form name="createDistrictForm" id="createDistrictForm" onsubmit="return createDistrict(event)">
                            <div class="form-group">
                                <label for="name">District Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="Test District">
                            </div>
                            <div class="form-group">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" id="country" name="country" value="Singapore">
                            </div>
                            <div class="form-group">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" name="city" value="Singapore">
                            </div>
                            <div class="form-group">
                                <label for="coords">Coords</label>
                                <textarea readonly type="textarea" class="form-control" id="coords" name="coords" rows="4"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary" id="submitBtn">
                                Submit
                            </button>
                            <button class="btn btn-secondary" id="resetBtn" onclick="return resetPolygon(event)">
                                Reset
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Async script executes immediately and must be after any DOM elements used in callback. -->
<script
src="https://maps.googleapis.com/maps/api/js?key={{ config('app.GOOGLE_MAP_API_KEY') }}&callback=initMap&libraries=&v=weekly"
async defer
></script>
@endsection
