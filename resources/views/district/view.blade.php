@extends('layouts.default')

@section('title')
Districts
@endsection

@section('pageScripts')
<script>
let coordJson = {!! $coords !!};
let coordsFromView = coordJson.coordinates;
</script>
<script src="{{ URL::asset('/js/district/view.js') }}"></script>
@endsection

@section('content')
<div class="row justify-content-center pt-4">
    <div class="col col-lg-8">
        <h1>View District</h1>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-lg-6">
                    <div id="map"></div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div>
                            <b>ID: </b><span>{{ $district->id }}</span><br/>
                            <b>Name: </b><span>{{ $district->name }}</span><br/>
                            <b>Country: </b><span>{{ $district->country }}</span><br/>
                            <b>City: </b><span>{{ $district->city }}</span><br/>
                            <b>Created At: </b><span>{{ $district->created_at->format('d M Y H:i:s') }}</span><br/>
                            <b>Updated At: </b><span>{{ $district->updated_at->format('d M Y H:i:s') }}</span><br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Async script executes immediately and must be after any DOM elements used in callback. -->
<script
src="https://maps.googleapis.com/maps/api/js?key={{ config('app.GOOGLE_MAP_API_KEY') }}&callback=initMap&libraries=&v=weekly"
async defer
></script>
@endsection
