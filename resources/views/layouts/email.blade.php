<html>
	<body>		
		<div class="wrapper" style='width:600px; font-family: Arial, sans-serif; font-size:14px;'>
			<div class="header">
            </div>
			@yield('content')
			<div class="footer" style="color:white; background-color:#0d7abf; text-align:center; padding:30px 0;">	
			</div>
		</div>
	</body>
</html>