@extends('layouts.default')

@section('title')
Where are my trucks now?
@endsection

@section('pageScripts')
<script async src="{{ URL::asset('/js/truck.js') }}"></script>
@endsection

@section('content')
<div class="row justify-content-center pt-4">
    <div class="col col-lg-8">
        <h1>Where are my trucks now?</h1>
        <div class="card">
            <div class="card-img-top" id="map"></div>
            <div class="card-body">
                <div id="truck-list"></div>
            </div>
        </div>
    </div>
</div>
<!-- Async script executes immediately and must be after any DOM elements used in callback. -->
<script
src="https://maps.googleapis.com/maps/api/js?key={{ config('app.GOOGLE_MAP_API_KEY') }}&callback=initMap&libraries=&v=weekly"
async defer
></script>
@endsection
