@extends('layouts.email')

@section('content')
<div class="container">
    <p>The following district is created</p>
    <ul>
        <li>Name: {{ $district->name }}</li>
        <li>Country: {{ $district->country }}</li>
        <li>Created On: {{ $district->created_at->format('Y-m-d H:i:s') }}</li>
    </ul>
</div>
@endsection