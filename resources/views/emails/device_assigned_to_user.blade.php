@extends('layouts.email')

@section('content')
<div class="container">
    <p>The following device is assigned to the user</p>
    <ul>
        <li>Device Key: {{ $device_key }}</li>
        <li>Device Value: {{ json_encode($device_value, true) }}</li>
        <li>User Email: {{ $user_email }}</li>
    </ul>
</div>
@endsection