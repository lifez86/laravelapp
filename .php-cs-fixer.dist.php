<?php
/*
 * This document has been generated with
 * https://mlocati.github.io/php-cs-fixer-configurator/#version:2.15|configurator
 * you can change this configuration by importing this file.
 */

$finder = PhpCsFixer\Finder::create()
        ->exclude('vendor')
        ->exclude('storage')
        ->in(__DIR__);

$config = new PhpCsFixer\Config();
return $config->setRiskyAllowed(true)
    ->setLineEnding("\r\n")
    ->setRules([
        '@Symfony' => true,
        '@PSR2' => true,
        '@PhpCsFixer' => true,
        'array_syntax' => ['syntax' => 'short'],
        'no_superfluous_phpdoc_tags' => false,
        'braces' => ['allow_single_line_closure' => true],
        'class_definition' => ['single_line' => true],
        'increment_style' => ['style' => 'post'],
        'logical_operators' => true,
        'multiline_whitespace_before_semicolons' => ['strategy' => 'no_multi_line'],
        'blank_line_before_statement' => ['statements' => ['return', 'throw']],
        'ordered_imports' => ['sort_algorithm' => 'length'],
        'yoda_style' => ['equal' => false, 'identical' => false, 'less_and_greater' => false],
    ])
    ->setFinder($finder);