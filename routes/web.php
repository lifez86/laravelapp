<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\DistrictWebController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@index');

Route::prefix('fakeapi')->group(function () {
    Route::get('/', 'Web\FakeApiWebController@index');
    Route::get('/store', 'Web\FakeApiWebController@store');
});

Route::prefix('districts')->group(function () {
    Route::get('/ajax-list', [DistrictWebController::class, 'listDistricts'])->name('district.ajax.list');

    Route::get('/', [DistrictWebController::class, 'index']);
    Route::get('/checker', [DistrictWebController::class, 'checker']);
    Route::get('/{district}', [DistrictWebController::class, 'show'])->name('district.show');
    Route::get('/{district}/edit', [DistrictWebController::class, 'edit'])->name('district.edit');
});

Route::prefix('trips')->group(function () {
    Route::get('/', 'Web\TripWebController@index');
});

Route::prefix('trucks')->group(function () {
    Route::get('/', 'Web\TruckWebController@index');
});
