<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware('throttle:500,1')->group(function () {
    Route::middleware('psk')->group(function ($router) {
        //place secure routes here in future
        Route::get('devices/get_all_records', 'DeviceController@index');
        Route::get('devices/{device_key}', 'DeviceController@show');
    });

    Route::put('devices/{device_key}/add-user', 'DeviceController@addUser');
    Route::post('devices', 'DeviceController@store');

    // Route::resource('commands', 'CommandController')->only([
    //     'index', 'show'
    // ]);

    //order of routes matters
    Route::get('commands/chunk', 'CommandController@chunk');
    Route::get('commands/subselect', 'CommandController@subselect');
    Route::get('commands', 'CommandController@index');
    Route::get('commands/{command}', 'CommandController@show');

    Route::post('users/{userId}/upload', 'UserController@uploadFile');
    Route::get('users/{userId}', 'UserController@show');
    Route::get('users', 'UserController@index');

    Route::prefix('districts')->group(function ($router) {
        Route::get('/list', 'DistrictController@list');
        Route::get('/', 'DistrictController@get');
        Route::post('/', 'DistrictController@store');
        Route::put('/{district}', 'DistrictController@update');
    });

    Route::get('trucks', 'TruckController@get');
    Route::post('trucks', 'TruckController@store');

    Route::get('trips', 'TripController@get');
    Route::get('trips/{trip}', 'TripController@show');

    Route::post('carstates', 'CarStateController@store');

    Route::middleware('auth:api')->prefix('fakeapi')->group(function ($router) {
        //place secure routes here in future
        Route::get('/', 'FakeApiController@index');
        Route::post('/store', 'FakeApiController@store');
    });

    Route::prefix('batch')->group(function ($router) {
        Route::post('/upload', 'BatchController@upload');
        Route::post('/upload-split', 'BatchController@uploadSplit');
        Route::get('/store', 'BatchController@store');
        Route::get('/get/{id}', 'BatchController@get');
    });
});

//user registration
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');

Route::fallback('DefaultController@notFound');
